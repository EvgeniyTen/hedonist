//
//  Core.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 10.05.2023.
//

import Foundation
import Combine
import CoreLocation

class Core {
    
    ///unused object
    @Published var currentLocation: CurrentValueSubject<CLLocationCoordinate2D, Never>
    @Published var currentAdress: CurrentValueSubject<UserAddress?, Never>
    ///
    
    @Published var userData: CurrentValueSubject<CustomerData, Never> = .init(.notLoggedUser)
    @Published var restaurants: CurrentValueSubject<[TripAdvisorRestaurantModel], Never>
    @Published var favoriteRestaurants: CurrentValueSubject<[TripAdvisorRestaurantModel], Never>
    @Published var restaurantProperties: CurrentValueSubject<[String: Any], Never>
    @Published var selectedFilters: CurrentValueSubject<[FilterCellModel], Never>
    @Published var notificationStatus: CurrentValueSubject<Bool, Never> = .init(false)

    let action: PassthroughSubject<Action, Never> = .init()
    var dispose = Set<AnyCancellable>()
    
    let locationManager: LocationServiceProtocol
    let loginManager: LoginManagerProtocol
    let fileManager: LocalFileManagerProtocol
    let persistanManager: CoreDataManagerProtocol
    let notificationsManager: NotificationsManagerProtocol
    let toastManager: ToastManagerProtocol
    
    init(locationManager: LocationServiceProtocol,
         loginManager: LoginManagerProtocol,
         fileManager: LocalFileManagerProtocol,
         persistanManager: CoreDataManagerProtocol,
         notificationsManager: NotificationsManagerProtocol,
         toastManager: ToastManagerProtocol
    ) {
        self.locationManager = locationManager
        self.loginManager = loginManager
        self.fileManager = fileManager
        self.persistanManager = persistanManager
        self.notificationsManager = notificationsManager
        self.toastManager = toastManager
        ///unused object
        self.currentLocation = .init(CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
        self.currentAdress = .init(nil)
        ///
        self.restaurants = .init([])
        self.favoriteRestaurants = .init([])
        self.restaurantProperties = .init([:])
        self.selectedFilters = .init([])
        
        bindAction()
    }
    
    private func bindAction() {
        getFavorites()
        getRestautantProperties()
        
        action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                    // Location Manager
                case _ as CoreModelAction.LocationAction.RequestPermissions:
                    askLocationPermission()
                    
                case _ as CoreModelAction.LocationAction.StartObserving:
                    handleLocationUpdatesStart()

                case _ as CoreModelAction.LocationAction.StopObserving:
                    locationManager.stop()

                case let payload as CoreModelAction.RestaurantManagerAction.UpdateFavItem:
                    updateFavItem(restaurant: payload.value)
                    
                case let payload as CoreModelAction.RestaurantManagerAction.GetNewItem:
                    getRestaurants(value: payload.value)
                    
                case _ as CoreModelAction.RestaurantManagerAction.GetProperties:
                    getRestautantProperties()
                   
                case let payload as CoreModelAction.RestaurantManagerAction.DeleteCurrent:
                    Task {
                        await deleteCurrent(filters: payload.valueFilters, restaurant: payload.valueRestaurant)
                    }
                                        
                case let payload as CoreModelAction.PersistantAction.UpdateValue:
                    updateFilters(filterName: payload.name, filter: payload.filter)

                case _ as CoreModelAction.PersistantAction.DeleteValue:
                    clearFilters()
                    
                case _ as CoreModelAction.Notifications.RequestPermissions:
                    sendRequest()
                
                case _ as CoreModelAction.Notifications.CheckPermissions:
                    checkStatus()
                    
                case _ as CoreModelAction.LoginAction.SignInEnvironmentActive:
                    loadUserData()
                    
                case _ as CoreModelAction.LoginAction.SignOut:
                    signOut()
                    
                case let payload as CoreModelAction.Toasts.ThrowToast:
                    throwToast(message: payload.message, error: payload.error)

                default:
                    break
                }
            }
            .store(in: &dispose)
        
        loginManager.userData
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] data in
                userData.value = data
            }
            .store(in: &dispose)
        
        locationManager.currentLocation
            .combineLatest(locationManager.currentAdress)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] location, adress in
                currentLocation.send(location)
                currentAdress.send(adress)
            }
            .store(in: &dispose)
        
        persistanManager.selectedFilters
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] filters in
                selectedFilters.value = filters
            }
            .store(in: &dispose)
    }
    
    func getRestaurants(value: [String : Any] = [:]) {
        Task {
           await getItems(filters: value)
        }
    }
}

extension Core {
    static var emptyMock: Core = {
        let locationManager = LocationService()
        let loginManager = LoginManager()
        let fileManager = LocalFileManager()
        let persistanManager = CoreDataManager()
        let notificationsManager = NotificationsManager()
        let toastManager = ToastManager(toastInfo: .init())
        let core = Core(locationManager: locationManager,
                        loginManager: loginManager,
                        fileManager: fileManager,
                        persistanManager: persistanManager,
                        notificationsManager: notificationsManager,
                        toastManager: toastManager
        )
        return core
    }()
}

enum CoreModelAction {
    
}
