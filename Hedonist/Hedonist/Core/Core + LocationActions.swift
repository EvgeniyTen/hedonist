//
//  Core + LocationActions.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import Foundation

extension CoreModelAction {
    
    enum LocationAction{
        struct RequestPermissions: Action {}
        struct StartObserving: Action {}
        struct StopObserving: Action {}
    }
}

extension Core {
    
    func askLocationPermission() {
        locationManager.requestPermissions()
    }
    
    func handleLocationUpdatesStart() {
        if locationManager.status.value == .disabled {
            locationManager.requestPermissions()
        }
        locationManager.start()
        locationManager.currentLocation
            .sink { [unowned self] location in
                currentLocation.send(location)
            }
            .store(in: &dispose)
        
    }
}
