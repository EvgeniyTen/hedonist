//
//  Core + LoginActions.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import Foundation

extension CoreModelAction {
    enum LoginAction{
        struct SignInEnvironmentActive: Action {}
        struct SignOut: Action {}
    }
}

extension Core {
    func loadUserData() {
        do {
            try loginManager.fetchDataFromsecuredContainer()
        } catch let error {
            throwToast(error: error)
        }
    }
    
    func signOut() {
        do {
            try loginManager.signOut()
        } catch let error {
            throwToast(error: error)
        }
    }
}
