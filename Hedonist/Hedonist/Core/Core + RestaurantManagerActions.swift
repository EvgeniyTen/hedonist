//
//  Core + RestaurantManagerActions.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 25.05.2023.
//

import Foundation

extension CoreModelAction {
    
    enum RestaurantManagerAction {
        struct GetRestaurantProperties: Action {}
        
        struct GetNewItem: Action {
            var value: [String : Any]
        }
        
        struct DeleteCurrent: Action {
            var valueFilters: [String : Any]
            var valueRestaurant: TripAdvisorRestaurantModel
        }
        
        struct UpdateFavItem: Action {
            var value: TripAdvisorRestaurantModel
        }
        
        struct GetProperties: Action {}
    }
}

extension Core {

    func getRestautantProperties() {
        fileManager.getRestautantProperties { [weak self] filters in
            self?.restaurantProperties.send(filters)
        }
    }
    
    func getItems(filters: [String : Any]) async {
        do {
            try await fileManager.getPLaces(filters) { [weak self] restaurant in
                self?.restaurants.send(restaurant)
            }
        } catch let error {
            throwToast(error: error)
        }
    }
    
    func deleteCurrent(filters: [String: Any], restaurant: TripAdvisorRestaurantModel) async {
        do {
            try fileManager.writeToFile(place: restaurant)
            try await fileManager.getPLaces(filters) { [weak self] restaurant in
                self?.restaurants.send(restaurant)
            }
        } catch let error {
            throwToast(error: error)
        }
    }
}
