//
//  Core + PersistantActions.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 31.01.2024.
//

import Foundation

extension CoreModelAction {
    
    enum PersistantAction {
        struct UpdateValue: Action {
            let name: String
            let filter: String
        }
        struct DeleteValue: Action {}
    }
}

extension Core {
    
    func updateFavItem(restaurant: TripAdvisorRestaurantModel) {
        do {
            try persistanManager.updateFavorite(by: restaurant.id)
            getFavorites()
        } catch let error {
            throwToast(error: error)
        }
    }
    
    func getFavorites() {
        do {
            try persistanManager.getfavorites { [weak self] favoriteIDs in
                guard let self else { return }
                var container: [TripAdvisorRestaurantModel] = []
                favoriteIDs.forEach { id in
                    if let restaurant = self.fileManager.restaurants.first(where: {$0.id == id}) {
                        container.append(restaurant)
                    }
                }
                favoriteRestaurants.send(container)
            }
        } catch let error {
            throwToast(error: error)
        }
    }
    
    func updateFilters(filterName: String, filter: String) {
        do {
            try persistanManager.update(filterName, filter: filter)
        } catch let error {
            throwToast(error: error)
        }
    }
    
    func clearFilters() {
        do {
            try persistanManager.clearAll()
        } catch let error {
            throwToast(error: error)
        }
    }
}
