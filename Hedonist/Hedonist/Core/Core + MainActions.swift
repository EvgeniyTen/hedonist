//
//  Core + MainActions.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 25.07.2023.
//

import Foundation

extension CoreModelAction {
    
    enum MainViewActions {
        struct FiltersTapped: Action {}
        struct FavoritesTapped: Action {}
        struct ProfileTapped: Action {}
    }
}
