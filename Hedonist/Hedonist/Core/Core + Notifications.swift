//
//  Core + Notifications.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 14.03.2024.
//

import Foundation

extension CoreModelAction {
    
    enum Notifications {
        struct RequestPermissions: Action {}
        struct CheckPermissions: Action {}

    }
}

extension Core {
    func sendRequest() {
        notificationsManager.checkStatus { status in
            if !status {
                self.notificationsManager.request { status in
                    self.notificationStatus.value = status
                }
            } else {
                self.notificationStatus.value = status
            }
        }
    }
    
    func checkStatus() {
        notificationsManager.checkStatus { status in
            self.notificationStatus.value = status
        }
    }
}
    
