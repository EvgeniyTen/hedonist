//
//  Core + Toasts.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 17.03.2024.
//

import Foundation

extension CoreModelAction {
    
    enum Toasts {
        struct ThrowToast: Action {
            let message: String?
            let error: Error?
        }
    }
}

extension Core {
    func throwToast( message: String? = nil, error: (any Error)? = nil) {
        if let message {
            toastManager.showMessage(message)
        }
        
        if let error {
            toastManager.throwError(error)
        }
    }
}
