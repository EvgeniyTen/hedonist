//
//  RestaurantModel.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 16.04.2023.
//

import Foundation

extension TripAdvisorRestaurantModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}

struct TripAdvisorRestaurantModel: Codable {
    let name: String
    let registeredAtTripadvisor: Bool?
    let rating, ratingFood, ratingPriceQuality, ratingService: Double?
    let city: String?
    let address: String?
    let zipcode: String?
    let country: String?
    let latitude, longitude: Double?
    let neighborhood, email, tel: String?
    let tripadvisorURL: String?
    let menuURL: String?
    let workingHoursByDays: [[String]?]?
    let priceLevelFrom, priceLevelTo: Int?
    let dietaryRestrictions: [String]
    let glutenFreeDishes, vegetarianFriendly, veganFriendly: Bool?
    let cuisines: [String]
    let priceRangeMin, priceRangeMax: Int?
    let priceRangeCurrency: PriceRangeCurrency?
    let reviewsCount: Int?
    let restaurantDescription: String?
    let eatingTimes: [EatingTime]?
    let features: [String]?
    let landmark: [Landmark]?
    let website: String?
    let rank, rankTotalCount: Int?
    let photoUrls: [String]?
    let id: Int

    enum CodingKeys: String, CodingKey {
        case name
        case registeredAtTripadvisor = "registered_at_tripadvisor"
        case rating
        case ratingFood = "rating_food"
        case ratingPriceQuality = "rating_price_quality"
        case ratingService = "rating_service"
        case city, address, zipcode, country, latitude, longitude, neighborhood, email, tel
        case tripadvisorURL = "tripadvisor_url"
        case menuURL = "menu_url"
        case workingHoursByDays = "working_hours_by_days"
        case priceLevelFrom = "price_level_from"
        case priceLevelTo = "price_level_to"
        case dietaryRestrictions = "dietary_restrictions"
        case glutenFreeDishes = "gluten_free_dishes"
        case vegetarianFriendly = "vegetarian_friendly"
        case veganFriendly = "vegan_friendly"
        case cuisines
        case priceRangeMin = "price_range_min"
        case priceRangeMax = "price_range_max"
        case priceRangeCurrency = "price_range_currency"
        case reviewsCount = "reviews_count"
        case restaurantDescription = "description"
        case eatingTimes = "eating_times"
        case features, landmark, website, rank
        case rankTotalCount = "rank_total_count"
        case photoUrls = "photo_urls"
        case id
    }
}


enum EatingTime: String, Codable {
    case бранч = "Бранч"
    case завтрак = "Завтрак"
    case напитки = "Напитки"
    case обед = "Обед"
    case открытоДопоздна = "Открыто допоздна"
    case ужин = "Ужин"
}

enum Landmark: Codable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(Landmark.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for Landmark"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

enum PriceRangeCurrency: String, Codable {
    case rub = "RUB"
}

extension TripAdvisorRestaurantModel {
    enum FilterType {
        case glutenFreeDishes
        case vegetarianFriendly
        case veganFriendly
    }
}

extension TripAdvisorRestaurantModel: Equatable {
    static func == (lhs: TripAdvisorRestaurantModel, rhs: TripAdvisorRestaurantModel) -> Bool {
        lhs.name == rhs.name &&
        lhs.address == rhs.address &&
        lhs.longitude == rhs.longitude &&
        lhs.latitude == rhs.latitude &&
        lhs.website == rhs.website
    }
    
    
}

extension TripAdvisorRestaurantModel {
    static let mock = TripAdvisorRestaurantModel(name: "Hfjskghklshdghbdshjbdskvsdffshd",
                                            registeredAtTripadvisor: false,
                                            rating: 4.0,
                                            ratingFood: nil,
                                            ratingPriceQuality: nil,
                                            ratingService: nil,
                                            city: "Москва",
                                            address: "Пушкина колотушкина",
                                            zipcode: "фваып",
                                            country: "ыфвпфывп",
                                            latitude: 55.717346,
                                            longitude: 37.78947,
                                            neighborhood: "Лохмачевский",
                                            email: "фывпфп",
                                            tel: "роыфвлдапр",
                                            tripadvisorURL: "",
                                            menuURL: "http://www.baravi-t.com_Zux",
                                            workingHoursByDays: [["10:00:00", "20:00:00"], ["08:00:00", "21:00:00"], ["08:00:00", "21:00:00"], ["08:00:00", "21:00:00"], ["08:00:00", "21:00:00"], ["08:00:00", "21:00:00"], ["10:00:00", "20:00:00"]],
                                            priceLevelFrom: 1,
                                            priceLevelTo: 1,
                                            dietaryRestrictions: [],
                                            glutenFreeDishes: false,
                                            vegetarianFriendly: false,
                                            veganFriendly: false,
                                            cuisines: [],
                                            priceRangeMin: 120,
                                            priceRangeMax: 1200,
                                            priceRangeCurrency: nil,
                                            reviewsCount: 0,
                                            restaurantDescription: "ashdgjkhopdsuig oher iughodsuioghv iudfoghvij odsfghiuoads gviuoydsa GVyiusdvgsydiuoV GYDSUIOGVIUYSOD GVIUODHSashdgjkhopdsuig oher iughodsuioghv iudfoghvij odsfghiuoads gviuoydsa GVyiusdvgsydiuoV GYDSUIOGVIUYSOD GVIUODHSVashdgjkhopdsuig oher iughodsuioghv iudfoghvij odsfghiuoads gviuoydsa GVyiusdvgsydiuoV GYDSUIOGVIUYSOD GVIUODHSVashdgjkhopdsuig oher iughodsuioghv iudfoghvij odsfghiuoads gviuoydsa GVyiusdvgsydiuoV GYDSUIOGVIUYSOD GVIUODHSVV",
                                            eatingTimes: [],
                                            features: ["Бронирование", "Места для сидения", "Детские стульчики для кормления", "Подают алкоголь", "Бар", "Вино и пиво", "Принимаются карты Mastercard", "Принимаются карты Visa", "Бесплатный Wi-Fi", "Принимаются кредитные карты", "Обслуживание посетителей за столиками"],
                                            landmark: [.string("Усадьба Кусково"), .integer(2003)],
                                            website: "http://www.baravi-t.com_Zux",
                                            rank: 1,
                                            rankTotalCount: 1,
                                            photoUrls: ["https://sun9-71.userapi.com/OTiA9MicXLXEjC5y70k1AJXmgKwuYxS6Q7nAVw/65Ao8mJKiH0.jpg"],
                                            id: 1)
    static let empty = TripAdvisorRestaurantModel(name: "",
                                            registeredAtTripadvisor: false,
                                            rating: 0.0,
                                            ratingFood: nil,
                                            ratingPriceQuality: nil,
                                            ratingService: nil,
                                            city: "",
                                            address: "",
                                            zipcode: "",
                                            country: "",
                                            latitude: 0,
                                            longitude: 0,
                                            neighborhood: "",
                                            email: "",
                                            tel: "",
                                            tripadvisorURL: "",
                                            menuURL: nil,
                                            workingHoursByDays: [],
                                            priceLevelFrom: 1,
                                            priceLevelTo: 1,
                                            dietaryRestrictions: [],
                                            glutenFreeDishes: false,
                                            vegetarianFriendly: false,
                                            veganFriendly: false,
                                            cuisines: [],
                                            priceRangeMin: nil,
                                            priceRangeMax: nil,
                                            priceRangeCurrency: nil,
                                            reviewsCount: 0,
                                            restaurantDescription: "",
                                            eatingTimes: [],
                                            features: [],
                                            landmark: [],
                                            website: "",
                                            rank: 1,
                                            rankTotalCount: 1,
                                            photoUrls: nil,
                                            id: 1)

}
