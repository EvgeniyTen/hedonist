//
//  SwipeDirections.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 04.02.2024.
//

import Foundation
enum SwipeDirection {
    case up
    case down
    case left
    case right
}
