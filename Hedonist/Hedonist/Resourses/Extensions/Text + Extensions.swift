//
//  Text + Extensions.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 17.03.2024.
//

import SwiftUI

public extension Text {
    init(_ attributedString: NSAttributedString) {
        self.init("") // initial, empty Text

        // scan the attributed string for distinctly attributed regions
        attributedString.enumerateAttributes(in: NSRange(location: 0, length: attributedString.length),
                                             options: []) { attrs, range, _ in
            let string: String = attributedString.attributedSubstring(from: range).string
            var text: Text = .init(string)

            // then, read applicable attributes and apply them to the Text

            if let font = attrs[.font] as? UIFont {
                // this takes care of the majority of formatting - text size, font family,
                // font weight, if it's italic, etc.
                text = text.font(.init(font))
            }

            if let color = attrs[.foregroundColor] as? UIColor {
                text = text.foregroundColor(Color(color))
            }

            if let kern = attrs[.kern] as? CGFloat {
                text = text.kerning(kern)
            }

            if let tracking = attrs[.tracking] as? CGFloat {
                text = text.tracking(tracking)
            }

            if let strikethroughStyle = attrs[.strikethroughStyle] as? NSNumber,
                strikethroughStyle != 0 {
                if let strikethroughColor = (attrs[.strikethroughColor] as? UIColor) {
                    text = text.strikethrough(true, color: Color(strikethroughColor))
                } else {
                    text = text.strikethrough(true)
                }
            }

            if let underlineStyle = attrs[.underlineStyle] as? NSNumber,
               underlineStyle != 0 {
                if let underlineColor = (attrs[.underlineColor] as? UIColor) {
                    text = text.underline(true, color: Color(underlineColor))
                } else {
                    text = text.underline(true)
                }
            }

            if let baselineOffset = attrs[.baselineOffset] as? NSNumber {
                text = text.baselineOffset(CGFloat(baselineOffset.floatValue))
            }
            // swiftlint: disable shorthand_operator
            // append the newly styled subtext to the rest of the text
            self = self + text
            // swiftlint: enable shorthand_operator
        }
    }
}

public extension NSAttributedString {
    struct HtmlConfig {
        let bodyFont: UIFont
        let h1Font: UIFont
        let h2Font: UIFont
        let h3Font: UIFont
        let h4Font: UIFont
        let color: UIColor
        let linkColor: UIColor
        public init(bodyFont: UIFont,
                    h1Font: UIFont,
                    h2Font: UIFont,
                    h3Font: UIFont,
                    h4Font: UIFont,
                    color: UIColor,
                    linkColor: UIColor) {
            self.bodyFont = bodyFont
            self.h1Font = h1Font
            self.h2Font = h2Font
            self.h3Font = h3Font
            self.h4Font = h4Font
            self.color = color
            self.linkColor = linkColor
        }
    }

    static func html(withBody body: String, config: HtmlConfig) -> NSAttributedString {
        // Match the HTML `lang` attribute to current localisation used by the app (aka Bundle.main).
        let bundle: Bundle = Bundle.main
        let lang: String = bundle.preferredLocalizations.first
            ?? bundle.developmentLocalization
            ?? "ru"
        func fontCSS(_ font: UIFont) -> String {
            """
                   font-family: \(font.fontName);
                   font-size: \(font.pointSize);
                   line-height: \(font.lineHeight)px;
                   """
        }

        if let data: Data =
         """
            <!doctype html>
            <html lang="\(lang)">
            <head>
                <meta charset="utf-8">
                <style type="text/css">
                    /*
                      Custom CSS styling of HTML formatted text.
                      Note, only a limited number of CSS features are supported by NSAttributedString/UITextView.
                    */

                    body {
                        \(fontCSS(config.bodyFont))
                        color: \(config.color.hex);
                    }

                    h1 {
                       \(fontCSS(config.h1Font))
                    }

                    h2 {
                       \(fontCSS(config.h2Font))
                    }

                    h3 {
                       \(fontCSS(config.h3Font))
                    }

                    h4, h5, h6 {
                       \(fontCSS(config.h4Font))
                    }

                    a {
                        color: \(config.linkColor.hex);
                    }

                    li:last-child {
                        margin-bottom: 1em;
                    }
                </style>
            </head>
            <body>

                \(body)

            </body>
            </html>
            """.data(using: .utf8),
           let result: NSAttributedString = try?
            .init(data: data,
                  options: [
                    .documentType: NSAttributedString.DocumentType.html,
                    .characterEncoding: NSUTF8StringEncoding
                  ],
                  documentAttributes: nil) {
            return result
        } else {
            return NSAttributedString(string: body)
        }
    }
}

// MARK: Converting UIColors into CSS friendly color hex string

private extension UIColor {
    var hex: String {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0

        getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        return String(
            format: "#%02lX%02lX%02lX%02lX",
            lroundf(Float(red * 255)),
            lroundf(Float(green * 255)),
            lroundf(Float(blue * 255)),
            lroundf(Float(alpha * 255))
        )
    }
}

extension NSAttributedString {
    static func html(withBody body: String,
                     color: UIColor = .black,
                     linkColor: UIColor = .blue) -> NSAttributedString {
        let config: NSAttributedString.HtmlConfig = .init(bodyFont: .systemFont(ofSize: 16),
                                                          h1Font: .systemFont(ofSize: 32),
                                                          h2Font: .systemFont(ofSize: 26),
                                                          h3Font: .systemFont(ofSize: 20),
                                                          h4Font:.systemFont(ofSize: 18),
                                                          color: color,
                                                          linkColor: linkColor
        )
        return Self.html(withBody: body.specToHTML(), config: config)
    }
}

extension String {
    func specToHTML() -> String {
        self
            .replacingOccurrences(of: "&lt;", with: "<")
            .replacingOccurrences(of: "&gt;", with: ">")
            .replacingOccurrences(of: "\r\n", with: "<br>")
    }
}
