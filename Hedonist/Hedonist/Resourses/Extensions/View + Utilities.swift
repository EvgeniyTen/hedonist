//
//  Utilities.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import SwiftUI

class Utilities {
    var userInterfaceStyle: ColorScheme? = .dark
    func overrideDisplayMode(_ value: Int) {
        var userInterfaceStyle: UIUserInterfaceStyle
        if value == 2 {
            userInterfaceStyle = .dark
        } else if value == 1 {
            userInterfaceStyle = .light
        } else {
            userInterfaceStyle = .unspecified
        }
        UIApplication.shared.windows.first?.overrideUserInterfaceStyle = userInterfaceStyle
    }
}

extension View {
    @ViewBuilder
    func disableListScroll(_ value: Bool) -> some View {
        if #available(iOS 16.0, *) {
            self
                .scrollDisabled(value)
        } else {
            if value {
                self
                    .simultaneousGesture(DragGesture(minimumDistance: 0), including: .all)
            }
        }
    }
}

public extension View {
    /// Applies the given transform if the given condition evaluates to `true`.
    /// - Parameters:
    ///   - condition: The condition to evaluate.
    ///   - transform: The transform to apply to the source `View`.
    /// - Returns: Either the original `View` or the modified `View` if the condition is `true`.
    @ViewBuilder
    func `if`<Content: View>(_ condition: Bool,
                             transform: (Self) -> Content) -> some View {
        if condition {
            transform(self)
        } else {
            self
        }
    }

    /// Closure given view if conditional.
    /// - Parameters:
    ///   - conditional: Boolean condition.
    ///   - truthy: Closure to run on view if true.
    ///   - falsy: Closure to run on view if false.
    @ViewBuilder
    func `if`<Truthy: View, Falsy: View>(_ conditional: Bool = true,
                                         @ViewBuilder _ truthy: (Self) -> Truthy,
                                         @ViewBuilder else falsy: (Self) -> Falsy) -> some View {
        if conditional {
            truthy(self)
        } else {
            falsy(self)
        }
    }

    /// Closure given view and unwrapped optional value if optional is set.
    /// - Parameters:
    ///   - conditional: Optional value.
    ///   - content: Closure to run on view with unwrapped optional.
    @ViewBuilder
    func iflet<Content: View, T>(_ conditional: T?,
                                 @ViewBuilder _ content: (Self, _ value: T) -> Content) -> some View {
        if let value = conditional {
            content(self, value)
        } else {
            self
        }
    }

    @ViewBuilder
    func iflet<Truthy: View, Falsy: View, T>(_ conditional: T?,
                                             @ViewBuilder _ truthy: (Self, _ value: T) -> Truthy,
                                             @ViewBuilder else falsy: (Self) -> Falsy) -> some View {
        if let value = conditional {
            truthy(self, value)
        } else {
            falsy(self)
        }
    }

    func handlePaginationWith(lastIndex: Int?,
                              action: @escaping () -> Void
    ) -> some View {
        let topPoint: CGFloat = UIScreen.main.bounds.height - 100
        let bottomPoint: CGFloat = UIScreen.main.bounds.height

        let triggerRange: Range<CGFloat> = topPoint..<bottomPoint
        return self.catchPosition { preferences in
            if let lastIndex,
               let banner = preferences.first(where: { $0.id == lastIndex }),
               triggerRange.contains(banner.absolutePosition),
               banner.id >= 5 {
                action()
            }
        }
    }

    func listPaddings(index: Data.Index, collectionCount: Int) -> some View {
        self
            .padding(.leading, leftPadding(index))
            .padding(.trailing, rightPadding(collectionCount, index))
    }

    private func leftPadding(_ index: Data.Index) -> CGFloat {
        CGFloat(index == 0 ? 16 : 0)
    }

    private func rightPadding(_ collectionCount: Int, _ index: Data.Index) -> CGFloat {
        CGFloat(index == collectionCount - 1 ? 16 : 0)
    }
}
