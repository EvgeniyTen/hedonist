//
//  View + Extensions.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 20.04.2023.
//

import SwiftUI
import Combine

extension View {
    var screenSize: CGRect {
       UIScreen.main.bounds
    }
    
    func screenWidth(by value: Double) -> CGFloat {
        UIScreen.main.bounds.width * value
    }
    
    func screenHeight(by value: Double) -> CGFloat {
        UIScreen.main.bounds.height * value
    }
    
    func gradientForeground(colors: [Color], startPoint: UnitPoint = .top, endPoint: UnitPoint = .bottom) -> some View {
        self.overlay(
            LinearGradient(
                colors: colors,
                startPoint: startPoint,
                endPoint: endPoint)
        )
            .mask(self)
    }
    
    public func popup<PopupContent: View>(
        isPresented: Binding<Bool>,
        view: @escaping () -> PopupContent) -> some View {
        self.modifier(
            Popup(
                isPresented: isPresented,
                view: view)
        )
    }
    
    func frameGetter(_ frame: Binding<CGRect>) -> some View {
        modifier(FrameGetter(frame: frame))
    }
    
    func handleVerticalSwipe(completion: @escaping (SwipeDirection) -> Void) -> some View {
        var offset: CGSize = .zero
        return self
            .highPriorityGesture(
                DragGesture()
                    .onChanged { gesture in
                        offset = gesture.translation
                    }
                    .onEnded { _ in
                        withAnimation {
                            handleSwipe(offset, completion)
                        }
                    }
            )
    }

    private func handleSwipe(_ off: CGSize, _ completion: @escaping (SwipeDirection) -> Void) {
        switch off.height {
        case -500...(0):
            completion(.up)
            return
        case 10...500:
            completion(.down)
            return
        default:
            break
        }
    }
    
    func toast(item: Binding<ToastViewModifier.Model?>) -> some View {
        modifier(ToastViewModifier(model: item))
    }
}

struct FrameGetter: ViewModifier {
    @Binding var frame: CGRect
    
    func body(content: Content) -> some View {
        content
            .background(
                GeometryReader { proxy -> AnyView in
                    let rect = proxy.frame(in: .global)
                    // This avoids an infinite layout loop
                    if rect.integral != self.frame.integral {
                        DispatchQueue.main.async {
                            self.frame = rect
                        }
                       }
                return AnyView(EmptyView())
            })
    }
}

struct ToastEnvironmentKey: EnvironmentKey {
    static var defaultValue: ToastGenerator = .init()
}

extension EnvironmentValues {
    var toastInfo: ToastGenerator {
        get { self[ToastEnvironmentKey.self] }
        set { self[ToastEnvironmentKey.self] = newValue }
    }
}

class ToastGenerator: ObservableObject {
    @Published var newValue: ToastViewModifier.Model?
    private var toCanc: AnyCancellable?
}

struct ToastViewModifier: ViewModifier {
    struct Model: Equatable {
        let title: String
        var subtitle: String?
        var type: ModelType = .error
    }
    
    enum ModelType {
        case error
        case message
    }
    @Environment(\.safeAreaInsets) private var safeAreaInsets
    @Binding private var model: Model?
    @State private var timer: Timer?
    @State var isPopupVisible: Bool = false
    
    init(model: Binding<Model?>) {
        self._model = .init(
            get: { model.wrappedValue == nil ? nil : model.wrappedValue },
            set: { newValue in
                if newValue == nil {
                    model.wrappedValue = nil
                }
            }
        )
        self.isPopupVisible = model.wrappedValue == nil
    }
    
    func body(content: Content) -> some View {
        content
            .popup(isPresented: $isPopupVisible) {
                popupContent
            }
            .animation(.easeInOut(duration: 0.25), value: model)
    }
    
    @ViewBuilder
    private var popupContent: some View {
        switch model?.type {
        case .error:
            errorContent
        case .message:
            messageContent
        case nil:
            EmptyView()
        }
    }
    
    @ViewBuilder
    private var messageContent: some View {
        if let model {
            VStack(alignment: .leading, spacing: 4) {
                Text(model.title).bold()

            }
            .foregroundStyle(.white)
            .padding()
            .frame(
                maxWidth: .infinity,
                maxHeight: 50,
                alignment: .leading)
            .background(.gray.opacity(0.7))
            .clipShape(.rect(cornerRadius: 20))
            .padding(.horizontal)
            .onTapGesture { self.model = nil }
            .onAppear {
                Task {
                    sleep(2)
                    self.model = nil
                }
            }
        }
    }
    
    @ViewBuilder
    private var errorContent: some View {
        if let model {
            VStack(alignment: .leading, spacing: 4) {
                Text(model.title).bold()
                if let subtitle = model.subtitle, !subtitle.isEmpty {
                    Text(subtitle)
                }
            }
            .foregroundStyle(.white)
            .padding(.top, safeAreaInsets.bottom > 0 ? 44 : 0)
            .padding()
            .frame(maxWidth: .infinity, alignment: .leading)
            .background(.red)
            .onTapGesture { self.model = nil }
            .transition(.move(edge: .top).combined(with: .opacity))
            .onAppear {
                Task {
                    sleep(2)
                    self.model = nil
                }
            }
        }

    }
}

private struct Show: ViewModifier {
    let isVisible: Bool

    @ViewBuilder
    func body(content: Content) -> some View {
        if isVisible {
            content
        }
    }
}

public extension View {
    func isVisible(_ condition: Bool) -> some View {
        self.modifier(Show(isVisible: condition))
    }
}
