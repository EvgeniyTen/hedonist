//
//  Array + Extensions.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 05.03.2024.
//

import Foundation

extension Array where Element: Hashable {
    func groupBy<Key: Hashable>(_ keyFunction: (Element) -> Key) -> [Key: [Element]] {
        Dictionary(grouping: self, by: keyFunction)
    }
}

extension Array where Element: Identifiable {
    func split(_ chunks: Int) -> [[Element]] {
        guard chunks > 0, !self.isEmpty else { return [] }
        
        let totalElements = self.count
        let chunkSize = totalElements / chunks
        var extraElements = totalElements % chunks
        
        var result: [[Element]] = []
        var currentIndex = 0
        
        for _ in 1...chunks {
            var currentChunkSize = chunkSize
            if extraElements > 0 {
                currentChunkSize += 1
                extraElements -= 1
            }
            
            let endIndex = currentIndex + currentChunkSize
            if endIndex <= totalElements {
                result.append(Array(self[currentIndex..<endIndex]))
            }
            
            currentIndex = endIndex
        }
        
        return result
    }
}
