//
//  String + Extensions.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 03.02.2024.
//

import Foundation

extension String {
    func asUnicode() -> String {
        let mutableString = NSMutableString(string: self)
        CFStringTransform(mutableString, nil, "Any-Hex/Java" as NSString, true)
        return mutableString as String
    }
}
