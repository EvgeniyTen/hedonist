//
//  HedonistApp.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 11.06.2023.
//

import SwiftUI
import SwiftData
import FirebaseAuth
import FirebaseCore
import FirebaseAppCheck

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        let providerFactory = AppCheckDebugProviderFactory()
        AppCheck.setAppCheckProviderFactory(providerFactory)
        FirebaseApp.configure()
        AppCheck.appCheck().token (forcingRefresh: false) { token, error in
            if let token = token {
                print ("AppCheck token: \(token.token), expiration date: \(token.expirationDate)")
            } else if let error = error {
                print ("AppCheck error: \(error as NSError).userInfo)")
            }
        }
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let firebaseAuth = Auth.auth()
        firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.unknown)
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let firebaseAuth = Auth.auth()
        
        if firebaseAuth.canHandleNotification(notification) {
            completionHandler(.noData)
            return
        }
    }
    
    
    func application(_ application: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return false
    }
}


@main
struct HedonistApp: App {
    @Environment(\.toastInfo) var toastInfo: ToastGenerator
    @State private var toastModel: ToastViewModifier.Model?
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    let core: Core = {
        let locationManager = LocationService()
        let loginManager = LoginManager()
        let fileManager = LocalFileManager()
        let persistanManager = CoreDataManager()
        let notificationsManager = NotificationsManager()
        let toastManager = ToastManager(toastInfo: .init())
        let core = Core(locationManager: locationManager,
                        loginManager: loginManager,
                        fileManager: fileManager,
                        persistanManager: persistanManager,
                        notificationsManager: notificationsManager,
                        toastManager: toastManager
        )
        return core
    }()
    
    var body: some Scene {
        WindowGroup {
            ContentView(core: core)
                .onOpenURL { url in
                    print("Received URL: \(url)")
                    Auth.auth().canHandle(url)
                }
                .onReceive(toastInfo.$newValue) { model in
                    toastModel = model
                }
                .toast(item: $toastModel)
        }
    }
}
