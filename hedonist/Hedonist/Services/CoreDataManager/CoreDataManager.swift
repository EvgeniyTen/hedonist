//
//  DataService.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 29.01.2024.
//

import Foundation
import CoreData
import Combine

enum UpdateType {
    case fav
    case fail
    case content
}

enum ContainerType {
    case favorites
    case filters
}

final class CoreDataManager {
    private let container: NSPersistentContainer
    private let containerName: String = "SavedFiltersContainer"
    private let filterEntityName = "FilterItem"
    private let favoriteEntityName = "FavoriteItem"
    
    private var savedFilterEntities = [FilterItem]()
    private var savedFavoritesEntities = [FavoriteItem]()

    private var filtersCollection: [FilterCellModel] = []
    private var favoritesCollection: [Int] = []
    
    @Published var selectedFilters: CurrentValueSubject<[FilterCellModel], Never> = .init([])

    init() {
        
        self.container = NSPersistentContainer(name: containerName)
        container.loadPersistentStores { [weak self] _, error in
            guard let self else { return }
            if let error = error {
                print("Error loading data: \(error.localizedDescription)")
            }
            loadCustomerInfo()
        }
    }

    private func applyChanges(to contaiter: ContainerType) throws {
        try container.viewContext.save()
        try loadSavedFilters()
        try loadSavedFavorites()
    }
    
    func loadSavedFavorites() throws {
        let request = NSFetchRequest<FavoriteItem>(entityName: favoriteEntityName)
        savedFavoritesEntities = try container.viewContext.fetch(request)
        favoritesCollection.removeAll()
        favoritesCollection = savedFavoritesEntities.map { Int($0.id) }
    }
    
    func loadCustomerInfo() {
        do {
            try loadSavedFavorites()
            try loadSavedFilters()
        } catch {}
    }
    
    func loadSavedFilters() throws {
        let request = NSFetchRequest<FilterItem>(entityName: filterEntityName)
        savedFilterEntities = try container.viewContext.fetch(request)
        filtersCollection.removeAll()
        savedFilterEntities.forEach { item in
            if let title = item.name,
               let filter = item.filter?.asUnicode(),
               let type = FilterCellType.init(rawValue: title){
                if let index = filtersCollection.firstIndex(where: {$0.title == title}) {
                    filtersCollection.remove(at: index)
                } else {
                    filtersCollection.append(FilterCellModel(isSelected: true, title: filter, type: type))
                }
            }
        }
        selectedFilters.send(Array(Set(filtersCollection)))
    }
}

extension CoreDataManager: CoreDataManagerProtocol {
    func update(_ filterName: String, filter: String) throws {
        if filterName == "neighborhood" {
            savedFilterEntities.indices.forEach { index in
                
                if savedFilterEntities.count - 1 >= index,
                   savedFilterEntities[index].name == filterName,
                   savedFilterEntities[index].filter != filter {
                    container.viewContext.delete(savedFilterEntities[index])
                    savedFilterEntities.remove(at: index)
                }
            }
            
            if filter == " Не выбрано" {
                return
            }
            
            let entity = FilterItem(context: container.viewContext)
            entity.id = UUID()
            entity.name = filterName
            entity.filter = filter
            try applyChanges(to: .filters)
            return
        }
        
        if let index = savedFilterEntities.firstIndex(where: { $0.filter == filter }) {
            container.viewContext.delete(savedFilterEntities[index])
            savedFilterEntities.remove(at: index)
        } else {
            let entity = FilterItem(context: container.viewContext)
            entity.id = UUID()
            entity.name = filterName
            entity.filter = filter
        }
        try applyChanges(to: .filters)
    }

    func clearAll() throws {
        savedFilterEntities.forEach { message in
            container.viewContext.delete(message)
        }
        filtersCollection.removeAll()
        try applyChanges(to: .filters)
    }

    func updateFavorite(by id: Int) throws {
        if let index = savedFavoritesEntities.firstIndex(where: { $0.id == id }) {
            container.viewContext.delete(savedFavoritesEntities[index])
            savedFavoritesEntities.remove(at: index)
        } else {
            let entity = FavoriteItem(context: container.viewContext)
            entity.id = Int16(id)
        }
        try applyChanges(to: .favorites)

    }

    func getfavorites(completion: @escaping ([Int]) -> Void) throws {
        try loadSavedFavorites()
        completion(favoritesCollection)
    }
}

