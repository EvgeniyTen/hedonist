//
//  CoreDataManagerProtocol.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 03.02.2024.
//

import Foundation
import Combine

protocol CoreDataManagerProtocol {
    func update(_ filterName: String, filter: String) throws
    func clearAll() throws
    func updateFavorite(by id: Int) throws
    func getfavorites(completion: @escaping ([Int]) -> Void) throws
    var selectedFilters: CurrentValueSubject<[FilterCellModel], Never> { get }
}
