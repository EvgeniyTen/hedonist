//
//  LocationManagerProtocol.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import Foundation
import Combine
import CoreLocation

protocol LocationServiceProtocol {
    
    var status: CurrentValueSubject<LocationAgentStatus, Never> { get }
    var currentLocation: CurrentValueSubject<CLLocationCoordinate2D, Never> { get }
    var currentAdress: CurrentValueSubject<UserAddress?, Never>  { get }

    func requestPermissions()
    func start()
    func stop()
}
