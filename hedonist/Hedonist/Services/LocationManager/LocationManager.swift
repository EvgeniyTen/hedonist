//
//  LocationManager.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import CoreLocation
import Combine
import MapKit

struct UserAddress {
    let adress: String
    let neighbor: String
}

final class LocationService: NSObject, ObservableObject, LocationServiceProtocol {
    
    var status: CurrentValueSubject<LocationAgentStatus, Never> = .init(.disabled)
    var currentLocation: CurrentValueSubject<CLLocationCoordinate2D, Never> = .init(CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    var currentAdress: CurrentValueSubject<UserAddress?, Never> = .init(nil)

    private let locationManager: CLLocationManager = CLLocationManager()
    private let geoCoder = CLGeocoder()
    private var dispose = Set<AnyCancellable>()
    
    override init() {
        super.init()
    }
    
    func requestPermissions() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        start()
        bind()
    }
    
    func start() {
        locationManager.startUpdatingLocation()
    }
    
    func stop() {
        locationManager.stopUpdatingLocation()
    }
    
    func showRealAddress(with location: CLLocation, completion: @escaping (UserAddress?) -> (Void)) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            
            guard let placemark = placemarks?.first else {
                _ = error?.localizedDescription ?? "Unexpected Error"
                return
            }
            
            let reversedGeoLocation = ReversedGeoLocation(with: placemark)
            if reversedGeoLocation.streetName == "" && reversedGeoLocation.streetNumber == "" {
                completion(nil)

            } else {
                completion(UserAddress(adress: reversedGeoLocation.formattedAddress, neighbor: reversedGeoLocation.state))
            }
        }
    }
}

//MARK: - Private Helpers

extension LocationService {
    
    func bind() {
        
        status.sink {[unowned self] status in
            
            if status == .disabled {
                currentLocation.value = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
            }
            
        }.store(in: &dispose)
    }
    
    func updateStatus(_ status: CLAuthorizationStatus) {
        self.status.value = .init(with: status)
    }
}

//MARK: - CLLocationManagerDelegate

extension LocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            showRealAddress(with: location) { [weak self] realAdress in
                self?.currentAdress.send(realAdress)
                self?.currentLocation.send(location.coordinate)
            }
        }
       }
    
    

       func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
           updateStatus(status)
       }
}


enum LocationAgentStatus {
    
    case disabled
    case available
    
    init(with status: CLAuthorizationStatus) {
        
        switch status {
        case .authorized, .authorizedAlways, .authorizedWhenInUse:
            self = .available
            
        default:
            self = .disabled
        }
    }
}


struct ReversedGeoLocation {
    let name: String            // eg. Apple Inc.
    let streetNumber: String    // eg. 1
    let streetName: String      // eg. Infinite Loop
    let city: String            // eg. Cupertino
    let state: String           // eg. CA
    let zipCode: String         // eg. 95014
    let country: String         // eg. United States
    let isoCountryCode: String  // eg. US
    
    var formattedAddress: String {
        return "\(streetNumber) \(streetName), \(city),\(country)"
    }
    
    var neighbor: String {
        return state 
    }
    // Handle optionals as needed
    init(with placemark: CLPlacemark) {
        self.name           = placemark.name ?? ""
        self.streetName     = placemark.thoroughfare ?? ""
        self.streetNumber   = placemark.subThoroughfare ?? ""
        self.city           = placemark.locality ?? ""
        self.state          = placemark.subAdministrativeArea ?? ""
        self.zipCode        = placemark.postalCode ?? ""
        self.country        = placemark.country ?? ""
        self.isoCountryCode = placemark.isoCountryCode ?? ""
    }
}
