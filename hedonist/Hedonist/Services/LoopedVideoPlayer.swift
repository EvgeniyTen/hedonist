//
//  LoopedVideoPlayer.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 12.05.2024.
//

import SwiftUI
import AVFoundation
import AVKit

struct EnhancedVideoPlayer<VideoOverlay: View>: View {
    @StateObject private var viewModel: ViewModel
    @ViewBuilder var videoOverlay: () -> VideoOverlay
    
    init(_ url: URL,
         endAction: EndAction = .none,
         @ViewBuilder videoOverlay: @escaping () -> VideoOverlay) {
        _viewModel = StateObject(wrappedValue: ViewModel(url: url, endAction: endAction))
        self.videoOverlay = videoOverlay
    }
    
    var body: some View {
        VideoPlayer(player: viewModel.player, videoOverlay: videoOverlay)
            .ignoresSafeArea()
    }
    
    class ViewModel: ObservableObject {
        let player: AVQueuePlayer
        
        init(url: URL, endAction: EndAction) {
            player = AVQueuePlayer(url: url)
            player.actionAtItemEnd = .none
            if endAction != .none {
                NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime,
                                                       object: nil,
                                                       queue: nil) { [self] notification in
                    let currentItem = notification.object as? AVPlayerItem
                    if endAction == .loop,
                       let currentItem = currentItem {
                        player.seek(to: CMTime.zero)
                        player.play()
                    }
                }
            }
            player.play()
        }
    }
    
    enum EndAction: Equatable {
        case none,
             loop
    }
}

extension EnhancedVideoPlayer where VideoOverlay == EmptyView {
  init(_ url: URL, endAction: EndAction) {
    self.init(url, endAction: endAction) {
      EmptyView()
    }
  }
}

#Preview {
    EnhancedVideoPlayer(Bundle.main.url(forResource: "hedonist", withExtension: "mov")!, endAction: .loop)
}
