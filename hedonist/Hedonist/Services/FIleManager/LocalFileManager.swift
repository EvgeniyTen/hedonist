//
//  LocalFileManager.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 25.05.2023.
//

import Foundation



final class LocalFileManager {
    private let fileManager = FileManager.default
    private let restsPath = Bundle.main.path(forResource: "MoscowRests", ofType: "json")
    
    private var filters: [String: Any] = [:]
    let excludedFeatures: [String] = [
        "Спорт-бары",
        "\"Алкоголь приносить с собой\"",
        "Принимаются карты Visa",
        "Для посетителей на автомобилях",
        "Услуги парковщика",
        "Рестораны для некурящих",
        "Принимаются только наличные",
        "Завтрак типа \"шведский стол\"",
        "Телевизор",
        "Принимаются кредитные карты",
        "Принимаются карты Discover",
        "Бесплатная внеуличная стоянка",
        "Обслуживание посетителей за столиками",
        "Пляж",
        "Принимаются карты Mastercard",
        "Принимаются карты American Express"
    ]
    var restaurants: [TripAdvisorRestaurantModel] = []

    init() {
        do {
            try self.fetchData()
        } catch {}
    }
}

extension LocalFileManager: LocalFileManagerProtocol {
    func writeToFile(place: TripAdvisorRestaurantModel) throws {
        guard let restsPath else { return }
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let fileUrl = URL(fileURLWithPath: restsPath)
        let places = restaurants.filter{ $0 != place }
        let jsonData = try encoder.encode(places)
        try jsonData.write(to: fileUrl)
        try fetchData()
    }
    
    func getPLaces(_ filters: [String : Any], completion: @escaping ([TripAdvisorRestaurantModel]) -> Void) async {
        self.filters = filters
        completion(filtered(restaurants))
    }
    
    func getRestautantProperties(completion: @escaping ([String: Any]) -> Void) {
        var properties = [String : Any]()

        let cuisines = Array(Set(restaurants
            .map { value in
                var values: [String] = []

                for cuisines in value.cuisines where cuisines != "" {
                    values.append(cuisines)
                }
                return values
            }
            .joined()
        ))
        
        let neighborhood = Array(Set(restaurants
            .map { value in
                var values: [String] = []
                
                if let value = value.neighborhood {
                    values.append(value)
                }
                if let value = value.city {
                    values.append(value)
                }
                return values
            }
            .joined()
        ))
        
        let dietaryRestrictions = Array(Set(restaurants
            .map { value in
                var values: [String] = []
                
                for restriction in value.dietaryRestrictions where restriction != "" {
                    values.append(restriction)
                }
                return values
            }
            .joined()
        ))
        
        let features = Array(Set(restaurants
            .map { value in
                var values: [String] = []
                guard let features = value.features else { return [String]() }
                for feature in features where feature != "" && !excludedFeatures.contains(feature) {
                    values.append(feature)
                }
                return values
            }
            .joined()
        ))
            
        properties["cuisines"] = cuisines
        properties["neighborhood"] = neighborhood
        properties["dietaryRestrictions"] = dietaryRestrictions
        properties["features"] = features

        completion(properties)
    }
}

private extension LocalFileManager {
    
    private func fetchData() throws {
        guard let restsPath else { return }
        let restsURL = URL(fileURLWithPath: restsPath)
        try collectPlaces(mainPath: restsURL)
    }
    
    private func collectPlaces(mainPath: URL?) throws {
        restaurants = try decodeData(pathName: mainPath)
    }
    
    private func decodeData(pathName: URL?) throws -> [TripAdvisorRestaurantModel] {
        var places: [TripAdvisorRestaurantModel] = []
        if let pathName = pathName {
            let jsonData = try Data(contentsOf: pathName)
            let decoder = JSONDecoder()
            let item = try decoder.decode([TripAdvisorRestaurantModel].self, from: jsonData)
            places = item
                .filter { $0.restaurantDescription != "" }
                .filter { $0.photoUrls != nil }
                .filter { $0.features != nil }
                .filter { $0.longitude != nil || $0.latitude != nil}
                .filter { $0.tel != nil}
                .filter { $0.rating != nil }


        }
        return places
    }
    
    
    private func filtered(_ isIncluded: [TripAdvisorRestaurantModel]) -> [TripAdvisorRestaurantModel] {
        var temporArray: [TripAdvisorRestaurantModel] = isIncluded
        
        for filter in filters {
            
            switch filter.key {
            case "neighborhood":
                if let values = filter.value as? [String] {
                    for exclude in values {
                        temporArray = temporArray.filter { $0.neighborhood == exclude || $0.city == exclude }
                    }
                }
                
            case "cuisines":
                if let values = filter.value as? [String] {
                    for exclude in  values {
                        temporArray = temporArray.filter{$0.cuisines.contains(where: {$0 == exclude})}
                    }
                }
                
            case "dietaryrestrictions":
                if let values = filter.value as? [String] {
                    for exclude in  values {
                        temporArray = temporArray.filter{$0.dietaryRestrictions.contains(where: {$0 == exclude})}
                    }
                }
            case "features":
                if let values = filter.value as? [String] {
                    for exclude in values {
                        temporArray = temporArray.filter{$0.features!.contains(where: {$0 == exclude})}
                    }
                }
                
            default:
                return []
            }
        }
        return temporArray
    }
}

