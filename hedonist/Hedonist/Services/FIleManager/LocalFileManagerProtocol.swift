//
//  FileManagerRequestProtocol.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 25.05.2023.
//

import Foundation
import Combine

protocol LocalFileManagerProtocol {
    var restaurants: [TripAdvisorRestaurantModel] { get }
    func getPLaces(_ filters: [String : Any], completion: @escaping ([TripAdvisorRestaurantModel]) -> Void) async throws
    func getRestautantProperties(completion: @escaping ([String: Any]) -> Void)
    func writeToFile(place: TripAdvisorRestaurantModel) throws
}
