//
//  ToastService.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 17.03.2024.
//

import SwiftUI

protocol ToastManagerProtocol {
    func throwError(_ error: Error)
    func showMessage(_ message: String)
}

class ToastManager: ToastManagerProtocol {
    private var toastInfo: ToastGenerator
    
    init(toastInfo: ToastGenerator) {
        self.toastInfo = toastInfo
    }
    
    func throwError(_ error: Error) {
        Task {
            await MainActor.run {
                if let nsError = error as? NSError,
                   nsError.domain == NSURLErrorDomain && nsError.code == NSURLErrorNotConnectedToInternet {
                    // Ошибка отсутствия интернета
                    toastInfo.newValue = .init(title: "Отсутствует интернет", subtitle: "Проверьте соединение и попробуйте снова")
                }

                if let error = error as NSError? {
                    // Дополнительная информация из userInfo, если она есть
                    if let detailedError = error.userInfo[NSLocalizedDescriptionKey] as? String {
                        toastInfo.newValue = .init(title: "Ошибка сохранения данных", subtitle: detailedError)
                    } else {
                        toastInfo.newValue = .init(title: "Ошибка сохранения данных", subtitle: error.localizedDescription)
                    }
                }
                
                if let localizedError = error as? LocalizedError, let errorDescription = localizedError.errorDescription {
                    // Если ошибка поддерживает LocalizedError и предоставляет пользовательское описание
                    toastInfo.newValue = .init(title: "Error", subtitle: errorDescription)
                } else {
                    // Обработка общего случая, если ошибка не является NSError и не предоставляет дополнительного описания
                    toastInfo.newValue = .init(title: "Что-то пошло не так", subtitle: error.localizedDescription)
                }
            }
        }
    }
    
    func showMessage(_ message: String) {
        Task {
            await MainActor.run {
                toastInfo.newValue = .init(title: message, type: .message)
            }
        }
    }
}
