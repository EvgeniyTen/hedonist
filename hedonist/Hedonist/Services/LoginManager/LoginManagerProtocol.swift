//
//  LoginManagerProtocol.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import Foundation
import AuthenticationServices
import Combine

protocol LoginManagerProtocol {
    var userData: CurrentValueSubject<CustomerData, Never> { get }
    func fetchDataFromsecuredContainer() throws
    func signOut() throws
    func appleLoginRequest(_ request: ASAuthorizationAppleIDRequest)
    func appleLoginHandler(_ result: Result<ASAuthorization, Error>, completion: (Bool) -> Void) throws
}
