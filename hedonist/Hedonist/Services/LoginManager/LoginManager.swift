//
//  LoginManager.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import SwiftUI
import AuthenticationServices
import KeychainManager
import Combine

struct CustomerData: Codable {
    var id ,name, surname: String
}
extension CustomerData {
    static let notLoggedUser = Self.init(id: "not_logged_user", name: "", surname: "not_logged_user")
}

final class LoginManager: LoginManagerProtocol {
    private let keychain: KeychainServiceProtocol
    var isLoading: CurrentValueSubject<Bool, Never> = .init(false)
    var userData: CurrentValueSubject<CustomerData, Never> = .init(.notLoggedUser)
    private var verificationID: String? = nil
    private var userName: String? = nil

    init() {
        self.keychain = KeychainService(serviceName: "hedonistContainer")
        do {
            try fetchDataFromsecuredContainer()
        } catch {}
    }

    func signOut() throws {
        try keychain.remove(for: "userData")
        try fetchDataFromsecuredContainer()
    }
    
    func fetchDataFromsecuredContainer() throws {
        if let data: CustomerData = try keychain.get(for: "userData") {
            userData.value = data
        } else {
            userData.value = .notLoggedUser
        }
    }
    
    func appleLoginRequest(_ request: ASAuthorizationAppleIDRequest) {
        request.requestedScopes = [.email, .fullName]
    }

    func appleLoginHandler(_ result: Result<ASAuthorization, Error>, completion: (Bool) -> Void) throws {
        switch result {
        case .success(let success):
            switch success.credential {
            case let credential as ASAuthorizationAppleIDCredential:
                let userID = credential.user
                let firstName = credential.fullName?.givenName ?? ""
                let lastName = credential.fullName?.familyName ?? ""
                try? keychain.set(
                    CustomerData(
                        id: userID,
                        name: firstName,
                        surname: lastName),
                    for: "userData")
                try fetchDataFromsecuredContainer()
                completion(true)
            default:
                break
            }
        case .failure(let failure):
            print(failure.localizedDescription)
            completion(false)
        }
    }
}
