//
//  NotificationsService.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 14.03.2024.
//

import Foundation
import Firebase
import FirebaseMessaging
import Combine

protocol NotificationsManagerProtocol {
    var token: CurrentValueSubject<String?, Never> { get }
    func checkStatus(completion: @escaping (Bool) -> Void)
    func request(completion: @escaping (Bool) -> Void)
}

public final class NotificationsManager: NSObject, NotificationsManagerProtocol {
    public var token: CurrentValueSubject<String?, Never> = .init(nil)

    
    private func register() {
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
    }

    func checkStatus(completion: @escaping (Bool) -> Void) {
        let notificationsManager = NotificationsManager()
        notificationsManager.checkAuthorizationStatus { authorized in
            completion(authorized)
        }
    }
    
    func request(completion: @escaping (Bool) -> Void) {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { success, error in
            guard error == nil else { return }
            if success {
                self.register()
                completion(success)
            }
        }
    }
}

extension NotificationsManager: UNUserNotificationCenterDelegate {
    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       willPresent notification: UNNotification) async
    -> UNNotificationPresentationOptions {
        let userInfo: [AnyHashable: Any] = notification.request.content.userInfo
        return [[.banner, .badge, .sound]]
    }

    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       didReceive response: UNNotificationResponse) async {
        let userInfo: [AnyHashable: Any] = response.notification.request.content.userInfo
    }
}

extension NotificationsManager: MessagingDelegate {
    public func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let dataDict: [String: String] = ["token": fcmToken ?? ""]
        token.value = fcmToken
    }
}

extension NotificationsManager {
    
    func checkAuthorizationStatus(completion: @escaping (Bool) -> Void) {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            // Проверяем статус разрешения
            if settings.authorizationStatus == .authorized {
                // Уведомления разрешены
                completion(true)
            } else {
                // Уведомления не разрешены
                completion(false)
            }
        }
    }
}
