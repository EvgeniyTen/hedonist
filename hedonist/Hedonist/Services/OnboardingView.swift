//
//  OnboardingView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 12.05.2024.
//

import SwiftUI
import AppTrackingTransparency
import Combine


enum OnboardingViewType {
    case description
    case att
    case location
}

final class OnboardingViewModel: ObservableObject {
    private let core: Core
    private var dispose = Set<AnyCancellable>()
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false

    @Published var viewType: OnboardingViewType
    @Published var descriptionBlock: TitleSubtitleBlock.ViewModel?
    @Published var topTitlesBlock: TitleSubtitleBlock.ViewModel?
    @Published var middleTitlesBlock: TitleSubtitleBlock.ViewModel?
    @Published var bottomTitlesBlock: TitleSubtitleBlock.ViewModel?
    @Published var formattedAddress: TitleSubtitleBlock.ViewModel?
    @Published var nextButton: HedonistButtonView.ViewModel?

    var utilities = Utilities()
    
    init(
        core: Core,
        viewType: OnboardingViewType = .description,
        descriptionBlock: TitleSubtitleBlock.ViewModel? = nil,
        topTitlesBlock: TitleSubtitleBlock.ViewModel? = nil,
        middleTitlesBlock: TitleSubtitleBlock.ViewModel? = nil,
        bottomTitlesBlock: TitleSubtitleBlock.ViewModel? = nil,
        formattedAddress: TitleSubtitleBlock.ViewModel? = nil,
        nextButton: HedonistButtonView.ViewModel? = nil
    ) {
        self.core = core
        self.viewType = viewType
        self.descriptionBlock = descriptionBlock
        self.topTitlesBlock = topTitlesBlock
        self.middleTitlesBlock = middleTitlesBlock
        self.bottomTitlesBlock = bottomTitlesBlock
        self.formattedAddress = formattedAddress
        self.nextButton = nextButton
        self.bind()
        self.nextButton = .init(title: "Далее", action: nextAction)
    }
    
    private func bind() {
        topTitlesBlock = .init(
            titleText: "Изучай новые заведения",
            titleColor: .white,
            titleImageName: "cutlery",
            subtitleText: "Список заведений актуален на текущий момент и будет периодически обновляться",
            subtitleImageName: nil)
        
        middleTitlesBlock = .init(
            titleText: "Подбери случайное место",
            titleColor: .white,
            titleImageName: "dices",
            subtitleText: "Настраивай фильтры по своим интересам для идеального совпадения",
            subtitleImageName: nil)
        
        bottomTitlesBlock = .init(
            titleText: "Сохраняй места в избранное",
            titleColor: .white,
            titleImageName: "heart",
            subtitleText: "Храни список своих любимых заведений в одном месте",
            subtitleImageName: nil)
        
        core.currentAdress
            .combineLatest(core.locationManager.status)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] adress, status in
                if status == .disabled {
                    formattedAddress = .init(titleText: "Ничего страшного", titleImageName: nil, subtitleText: "🥲 Для определения геопозиции можно будет воспользоваться списком предусмотренных нами локаций", subtitleImageName: nil)
                }

                if let fullAddress = adress?.adress,
                   let neighbor = adress?.neighbor {
                    formattedAddress = .init(
                        titleText: "Ваш адрес:",
                        titleColor: .white,
                        titleImageName: nil,
                        subtitleText: fullAddress,
                        subtitleImageName: nil
                    )
                }
            }
            .store(in: &dispose)
    }
    
    func nextAction() {
        switch viewType {
        case .description:
            topTitlesBlock = nil
            middleTitlesBlock = nil
            bottomTitlesBlock = nil
            viewType = .att
            ATTrackingManager.requestTrackingAuthorization { status in }
            descriptionBlock = .init(
                titleText: "Для улучшения пользовательского опыта.",
                titleColor: .white,
                titleImageName: nil,
                subtitleText: "Аналитика нужна только нам, поэтому нам неважно, какое ты примешь решение. Мы его уважаем! ☺️",
                subtitleImageName: nil)
            
        case .att:
            viewType = .location
            core.askLocationPermission()
            descriptionBlock = .init(
                titleText: "Для самых точных рассчетов.",
                titleColor: .white,
                titleImageName: nil,
                subtitleText: "Нам потребуется информация о твоей локации для сбора ресторанов рядом с тобой.🙃",
                subtitleImageName: nil)

        case .location:
            link = .filter(.init(core: core))
        }
    }
    
    
    enum Link {
        case filter(FilterViewModel)
    }
    
}

struct OnboardingView: View {
    @ObservedObject var viewModel: OnboardingViewModel
    
    var body: some View {
        ZStack(alignment: .bottom) {
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                if let link = viewModel.link  {
                    switch link {
                    case .filter(let viewModel):
                        FilterView(viewModel: viewModel)
                            .transition(.opacity)
                    }
                }
            }

            EnhancedVideoPlayer(Bundle.main.url(forResource: "hedonist", withExtension: "mp4")!, endAction: .loop)
            Color.black
                .opacity(0.5)
            VStack {
                VStack {
                    if let description = viewModel.descriptionBlock {
                        TitleSubtitleBlock(viewModel: description)
                    }
                    
                    if let top = viewModel.topTitlesBlock,
                       let middle = viewModel.middleTitlesBlock,
                       let bottom = viewModel.bottomTitlesBlock {
                        TitleSubtitleBlock(viewModel: top)
                        TitleSubtitleBlock(viewModel: middle)
                        TitleSubtitleBlock(viewModel: bottom)
                    }
                }
                .padding()
                .frame(maxWidth: .infinity)
                .padding(.horizontal)
                .background(materialBackGround)
                
                if let nextButton = viewModel.nextButton {
                    HedonistButtonView(viewModel: nextButton)
                        .padding(.horizontal)
                }
                
                Text("powered by TripAdvizor")
                    .frame(height: 100)
                    .font(.caption)
                    .foregroundColor(.white)
            }
        }
        .animation(.bouncy, value: viewModel.viewType)
        .ignoresSafeArea()

    }
}

#Preview {
    OnboardingView(viewModel: .init(core: .emptyMock))
}


extension OnboardingView {
    var materialBackGround: some View {
        Rectangle()
            .background(.ultraThinMaterial)
            .clipShape(.rect(cornerRadius: 25))
            .padding(.horizontal)
    }
}
