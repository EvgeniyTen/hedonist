//
//  LoginTypeSelectionView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 14.01.2024.
//

import SwiftUI
import Combine
import AuthenticationServices

extension LoginTypeSelectionView {
    class ViewModel: ObservableObject {
        private let core: Core
        var successDismiss: () -> Void
        var dismiss: (() -> Void)?

        @Published var showPhoneRegistrationView: Bool = false
        @Published var showErrorAlert: Bool = false
        @Published var closeButton: HedonistButtonView.ViewModel?

        init(core: Core,
             successDismiss: @escaping () -> Void,
             dismiss: (() -> Void)?,
             closeButton: HedonistButtonView.ViewModel?
        ) {
            self.core = core
            self.successDismiss = successDismiss
            self.dismiss = dismiss
            self.closeButton = closeButton
        }
        
        convenience init(core: Core,
                         successDismiss: @escaping () -> Void,
                         dismiss: (() -> Void)? = nil) {
            self.init(core: core,
                      successDismiss: successDismiss,
                      dismiss: dismiss,
                      closeButton: .init(title: "Закрыть", titleColor: .gray, color: .clear, action: {}))
            self.closeButton?.action = showNextScreen
        }
                
        func showNextScreen() {
            if let dismiss {
                dismiss()
            } else {
                self.core.action.send(CoreModelAction.LoginAction.SignInEnvironmentActive())
                self.core.action.send(LoginViewAction.LoginTapped())
            }
        }
        
        func showPhoneRegistration() {
            showPhoneRegistrationView = true
        }
        
        func request(_ request: ASAuthorizationAppleIDRequest) {
            core.loginManager.appleLoginRequest(request)
        }
        
        func login(_ result: Result<ASAuthorization, Error>) {
            do {
                try core.loginManager.appleLoginHandler(result) { [weak self] success in
                    guard let self = self else {return}
                    if success {
                        showNextScreen()
                        successDismiss()
                    } else {
                        showErrorAlert = true
                    }
                }
            } catch let error {
                core.action.send(CoreModelAction.Toasts.ThrowToast(message: nil, error: error))
            }
        }
        func hideKeyBoard() {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder),
                                            to: nil,
                                            from: nil,
                                            for: nil)
        }
    }
}
struct LoginTypeSelectionView: View {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack {
            RoundedRectangle(cornerRadius: 20)
                .frame(width: 50, height: 5)
                .opacity(0.5)
            VStack {
                SignInWithAppleButton(.continue, onRequest: viewModel.request) { result in
                    viewModel.login(result)
                }
                .frame(height: screenHeight(by: 0.06))
                .cornerRadius(20)
                .signInWithAppleButtonStyle(.white)
                
                if let closeButton = viewModel.closeButton {
                    HedonistButtonView(viewModel: closeButton)
                        .frame(height: screenHeight(by: 0.06))
                        .cornerRadius(20)
                }
            }
            .animation(.snappy, value: viewModel.showPhoneRegistrationView)
            .padding(20)
            .background(
                RoundedRectangle(cornerRadius: 20)
                    .fill(.gray.opacity(0.2))
                    .ignoresSafeArea()
            )
        }

    }
}

#Preview {
    LoginTypeSelectionView(viewModel: .init(core: .emptyMock, successDismiss: {}, dismiss: {}))
}
