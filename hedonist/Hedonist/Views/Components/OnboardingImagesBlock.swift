//
//  OnboardingImagesBlock.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 10.05.2023.
//

import SwiftUI

struct OnboardingImagesBlock: View {
    
    var imagesArray = ["rest1", "rest2", "rest3", "rest4", "rest5", "rest6", "rest7", "rest8"]
    
    var body: some View {
        HStack(alignment: .top, spacing: 20) {
            Image(imagesArray[5])
                .resizable()
                .imageScale(.large)
                .scaledToFill()
                .frame(width: screenWidth(by: 0.3), height: screenHeight(by: 0.25))
                .cornerRadius(10)
                .padding(.top, 10)
            VStack(spacing: 50) {
                ForEach(0..<2, id: \.self) { index in
                    Image(imagesArray[index])
                        .resizable()
                        .scaledToFill()
                        .imageScale(.large)
                        .frame(width: screenWidth(by: 0.31), height: screenHeight(by: 0.2))
                        .cornerRadius(10)
                }
            }
            VStack(spacing: 10) {
                ForEach(2...4, id: \.self) { index in
                    Image(imagesArray[index])
                        .resizable()
                        .imageScale(.large)
                        .scaledToFill()
                        .frame(width: screenWidth(by: 0.34), height: screenHeight(by: 0.18))
                        .cornerRadius(10)
                }
            }
            VStack(spacing: 10) {
                ForEach(6...7, id: \.self) { index in
                    Image(imagesArray[index])
                        .resizable()
                        .imageScale(.large)
                        .scaledToFill()
                        .frame(width: screenWidth(by: 0.3), height: screenHeight(by: 0.22))
                        .cornerRadius(10)
                }
            }
        }
    }
}

struct OnboardingImagesBlock_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingImagesBlock()
    }
}

