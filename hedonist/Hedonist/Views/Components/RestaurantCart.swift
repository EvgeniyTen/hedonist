//
//  DetailedCardView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 26.05.2023.
//

import SwiftUI
import MapKit
import Kingfisher

extension DetailedCardView {
    func isCurrentlyOpen() -> Bool? {
        let now = Date()
        let calendar = Calendar.current
        let weekDay = calendar.component(.weekday, from: now) // Воскресенье = 1, Понедельник = 2, и так далее
        let hour = calendar.component(.hour, from: now)
        let minute = calendar.component(.minute, from: now)
        
        // Находим рабочее время для текущего дня
        guard let workingDaysCount = place.workingHoursByDays?.count,
              workingDaysCount == 7,
              weekDay - 1 < workingDaysCount, // Убедитесь, что индекс дня недели валиден
              let todayWorkingHours = place.workingHoursByDays?[weekDay - 1]
        else { return nil }
        let openTimeComponents = todayWorkingHours[0].split(separator: ":").compactMap { Int($0) }
        let closeTimeComponents = todayWorkingHours[1].split(separator: ":").compactMap { Int($0) }
        
        // Корректировка времени закрытия
        let adjustedCloseHour = (closeTimeComponents[0] < openTimeComponents[0]) ? closeTimeComponents[0] + 24 : closeTimeComponents[0]
        
        // Создаем компоненты времени для сравнения
        let openingTime = DateComponents(hour: openTimeComponents[0], minute: openTimeComponents[1])
        let closingTime = DateComponents(hour: adjustedCloseHour, minute: closeTimeComponents[1])
        
        // Текущее время в минутах с начала дня
        let currentTimeInMinutes = hour * 60 + minute
        // Время открытия и закрытия в минутах с начала дня
        let openingTimeInMinutes = openingTime.hour! * 60 + openingTime.minute!
        let closingTimeInMinutes = closingTime.hour! * 60 + closingTime.minute!
        
        // Сравниваем
        return currentTimeInMinutes >= openingTimeInMinutes && currentTimeInMinutes < closingTimeInMinutes
    }
    
    func updateFavStateAction() async {
        DispatchQueue.main.async {
            self.isFavorite.toggle()
        }
       await favAction(place)
    }
}

struct DetailedCardView<Card: View>: View {
    
    @State private var isUserInformed: Bool = false
    @State private var isAdressInformerShowed: Bool = false
    @State private var isExpanded: Bool = false
    @State private var isScrolled2Top: Bool = false
    @State private var offset: CGSize = .zero
    @State var isFavorite: Bool = false
    @Environment(\.dismiss) var dismiss

    let place: TripAdvisorRestaurantModel
    let description: TitleSubtitleBlock.ViewModel
    let adressLine: RestaurantAdressCart.ViewModel
    let action: () -> Void
    let favAction: (TripAdvisorRestaurantModel) async -> Void
    @ViewBuilder var card: () -> Card
    
    init(place: TripAdvisorRestaurantModel,
         action: @escaping () -> Void,
         favAction: @escaping (TripAdvisorRestaurantModel) async -> Void,
         card: @escaping () -> Card
    ) {
        self.card = card
        self.place = place
        
        self.description = .init(
            titleText: place.address ?? "",
            titleColor: .white,
            titleImageName: nil,
            subtitleText: place.neighborhood ?? "",
            subTitleColor: .white.opacity(0.5),
            subtitleImageName: nil)
        
        self.adressLine = .init(
            textColor: .black,
            phoneNumber: place.tel ?? "",
            latitude: place.latitude,
            longitude: place.longitude,
            address: place.address ?? "")
        
        self.action = action
        self.favAction = favAction
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 20) {
                    card()
                        .frame(height: screenHeight(by: 0.4))
                    Text(place.name)
                        .foregroundColor(.black)
                        .font(.system(size: 25, weight: .semibold))
                        .multilineTextAlignment(.center)
                        .foregroundColor(place.photoUrls == nil ? .black : .white)
                        .frame(maxWidth: .infinity, alignment: .center)
                        .readGeometryPosition(index: 101)
                    .padding(.horizontal, 10)
                    
                    HStack {
                        Text("О заведении")
                            .foregroundColor(.black)
                            .font(.system(size: 18, weight: .bold))
                            .frame(maxWidth: .infinity, alignment: .leading)
                       
                    }
                    .padding(.horizontal, 10)

                    if let description = place.restaurantDescription {
                        Text(description)
                            .foregroundColor(.gray)
                            .font(.system(size: 14, weight: .regular))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .lineLimit(isExpanded ? 50 : 5)
                            .padding(.horizontal, 10)
                        if description.count > 160 {
                            Button {
                                isExpanded.toggle()
                            } label: {
                                Text(isExpanded ? "Скрыть" : "Показать еще")
                                    .foregroundColor(.gray)
                                    .font(.system(size: 12, weight: .light))
                                    .padding(.horizontal)
                                    .padding(.vertical, 5)
                                    .background(
                                        RoundedRectangle(cornerRadius: 10)
                                            .foregroundStyle(.gray.opacity(0.1))
                                    )
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            }
                            .padding(.horizontal, 10)
                        }
                    }
                    
                    if let state = isCurrentlyOpen() {
                        Text(state ? "Сейчас открыто" : "Сейчас закрыто")
                            .font(.system(size: 15))
                            .foregroundStyle(.white)
                            .padding(.horizontal)
                            .padding(.vertical, 5)
                            .background {
                                Capsule(style: .continuous)
                                    .foregroundStyle(state ? .green : .red)
                            }
                    }
                    
                    if let schedule = place.workingHoursByDays {
                        ScheduleView(workingHours: schedule)
                            .padding(.horizontal, 10)
                    }

                    if let priceRange = place.priceLevelTo {
                        HStack {
                            Text("Ценовая категория: ")
                                .foregroundColor(.black)
                                .font(.system(size: 18, weight: .bold))
                                .frame(maxWidth: .infinity, alignment: .leading)
                            RatingBar(.price(Double(priceRange)))
                                .frame(alignment: .trailing)
                        }
                        .padding(.horizontal, 10)
                    }
                    
                    if let lowPrice = place.priceRangeMin,
                       let highPrice = place.priceRangeMax {
                        VStack(alignment: .leading, spacing: 10) {
                            HStack {
                                Text("Средний чек:")
                                    .foregroundColor(.gray)
                                    .font(.system(size: 14, weight: .regular))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                HStack {
                                    Text("\(lowPrice)₽")
                                        .foregroundStyle(.gray)
                                        .font(.system(size: 16))
                                        .frame(alignment: .leading)
                                    
                                    Rectangle()
                                        .frame(width: 5, height: 1)
                                        .foregroundStyle(.gray)
                                    
                                    Text("\(highPrice)₽")
                                        .foregroundStyle(.gray)
                                        .font(.system(size: 16))
                                }
                                .frame(maxWidth: .infinity, alignment: .trailing)
                            }
                            if let menuURLString = place.menuURL,
                               let menuURL = URL(string: menuURLString) {
                                Link(destination: menuURL) {
                                    Text("Смотреть меню ")
                                        .foregroundColor(.gray)
                                        .font(.system(size: 12, weight: .light))
                                        .padding(.horizontal)
                                        .padding(.vertical, 5)
                                        .background(
                                            RoundedRectangle(cornerRadius: 10)
                                                .foregroundStyle(.gray.opacity(0.1))
                                        )
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                }
                            }
                        }
                        .padding(.horizontal, 10)
                    }
                    if !place.cuisines.isEmpty {
                        VStack(alignment: .leading) {
                            Text("Тип Кухни")
                                .padding(.top, 20)
                                .foregroundColor(.black)
                                .font(.headline)
                                .padding(.horizontal)
                            TagView(tags: place.cuisines)
                        }.padding(.horizontal, 10)
                    }
                    
                    if !place.dietaryRestrictions.isEmpty {
                        VStack(alignment: .leading) {
                            Text("Особенности")
                                .padding(.top, 20)
                                .foregroundColor(.black)
                                .font(.headline)
                                .padding(.horizontal)
                            TagView(tags: place.dietaryRestrictions)
                        }.padding(.horizontal, 10)
                    }

                    if let features = place.features,
                       !features.isEmpty {
                        VStack(alignment: .leading, spacing: 10) {
                            Text("Удобства:")
                                .foregroundColor(.black)
                                .font(.system(size: 18, weight: .bold))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.horizontal, 10)
                            TagView(tags: features)
                        }.padding(.horizontal, 10)
                    }
                    
                    if let adress = place.address,
                       let city = place.city {
                        VStack(alignment: .leading, spacing: 10) {
                            Text("Адрес:")
                                .foregroundColor(.black)
                                .font(.system(size: 18, weight: .bold))
                                .frame(maxWidth: .infinity, alignment: .leading)
                            HStack {
                                Text(city)
                                    .foregroundColor(.gray)
                                    .font(.system(size: 16, weight: .bold))
                                    .frame(alignment: .leading)
                                Rectangle()
                                    .frame(width: 1)
                                Text(adress)
                                    .foregroundColor(.gray)
                                    .font(.system(size: 14, weight: .regular))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            }
                            
                            if let neighbor = place.neighborhood {
                                HStack {
                                    Circle()
                                        .frame(height: 10)
                                        .foregroundStyle(.gray)
                                    Text(neighbor)
                                        .foregroundStyle(.gray)
                                        .font(.system(size: 16))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                }
                            }
                        }
                        .padding(.horizontal, 10)
                    }
                    
                    if let latitude = place.latitude,
                       let longitude = place.longitude {
                        ZStack(alignment: .center) {
                            MapView(
                                latitude: latitude,
                                longitude: longitude,
                                markers: [PointOfInterest(
                                    latitude: latitude,
                                    longitude: longitude
                                )]) { _ in }
                            .allowsHitTesting(false)
                            Color.white.opacity(0.7)
                            RestaurantAdressCart(
                                viewModel: adressLine,
                                isUserInformed: $isUserInformed
                            ) {
                                isAdressInformerShowed = true
                                isUserInformed = true
                            }
                                .padding(50)
                        }
                        .frame(maxWidth: .infinity,
                               minHeight: 300,
                               alignment: .bottom)
                        .clipShape(RoundedRectangle(cornerRadius: 20))
                    }
                    
                }
                .background(.white)
                .clipShape(.rect(cornerRadius: 20))
                .clipShape(.rect(cornerRadius: 20))
            }
            topBarContent
                .frame(maxHeight: .infinity, alignment: .top)
        }
        .catchPosition { preferences in
            var container = [Int: Double]()
            preferences.forEach {
                container[$0.id] = $0.absolutePosition > 0 ? $0.absolutePosition : 0
            }
            
            isScrolled2Top = container[101] ?? 0 <= 100
        }
        .ignoresSafeArea()
        .animation(.spring(response: 0.3, dampingFraction: 1), value: isExpanded)
        .animation(.spring(response: 0.3, dampingFraction: 1), value: isScrolled2Top)
        .navigationBarBackButtonHidden()
        .navigationBarHidden(true)
        .popup(isPresented: $isAdressInformerShowed) {
            adresInformer
                .padding()
        }
    }
    
    func handleSwipe(_ off: CGSize) {
        switch off.height {
        case -500...(0):
            changeDetailVisibility(true)
            return
        case 10...500:
            changeDetailVisibility(false)
            return
        default:
            offset = .zero
        }
    }
    
    func changeDetailVisibility(_ state: Bool) {
        isExpanded = false
        action()
    }
}

extension DetailedCardView {
    var adresInformer: some View {
        VStack {
            Text("Некоторые рестораны не сообщили нам о том, что они закрылись :(")
                .fontWeight(.bold)
                .font(.title3)
                .multilineTextAlignment(.leading)
                .frame(maxWidth: .infinity, alignment: .leading)
                .fontDesign(.rounded)

            Text("Настоятельно просим Вас убедиться в том, что это заведение работает")
                .fontWeight(.black)
                .foregroundStyle(Color(.themeOrange))
                .multilineTextAlignment(.leading)
                .frame(maxWidth: .infinity, alignment: .leading)
            PhoneButton(title: "Позвонить", phoneNumber: place.tel ?? "", titleColor: .black)
        }
        .padding()
        .overlay(alignment: .topTrailing) {
            Button {
                isAdressInformerShowed = false
                isUserInformed = true
            } label: {
                Image(systemName: "xmark")
                    .foregroundColor(.black)
            }
        }
        .padding(20)
        .background(.ultraThinMaterial)
        .clipShape(.rect(cornerRadius: 12))

    }
    
    private var topBarContent: some View {
        HStack(alignment: .top) {
            backButton
            Spacer()
            favButton
        }
        .padding()
        .padding(.top, screenHeight(by: 0.05))
        .background(isScrolled2Top ? .black : .clear)
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: .top)
        
    }
    
    private var backButton: some View {
        Button {
            changeDetailVisibility(false)
            dismiss()
        } label: {
            Image(.backIcon)
                .renderingMode(.original)
                .frame(width: 25, height: 25)
                .scaledToFit()
        }
    }
    
    private var favButton: some View {
        Button {
            Task {
                await updateFavStateAction()
            }
        } label: {
            Image(isFavorite ? .favSelectIcon : .favUnselectIcon)
        }
    }
}

#Preview("Main card") {
    RegularCardView(place: .mock)
}

#Preview("Detailed card") {
    DetailedCardView(place: .mock, action: {}, favAction: {_ in}) {
        RegularCardView(place: .mock, cornersRaduis: 0, isDetailed: true)
    }
}

struct RegularCardView: View {
    let place: TripAdvisorRestaurantModel
    @State var cornersRaduis: CGFloat = 20
    @State var isDetailed: Bool = false
    var body: some View {
        cardContent
            .background(
                cardImage
                    .transition(.opacity.animation(.easeInOut))
                    .scaledToFill()
            )
            .clipShape(RoundedRectangle(cornerRadius: cornersRaduis))
    }
}

extension RegularCardView {
    
    @ViewBuilder
    var cardImage: some View {
        if let urlString = place.photoUrls?[0],
           let url = URL(string: urlString) {
            KFImage(url)
                .placeholder {
                    ProgressView()
                }
                .resizable()
                .aspectRatio(contentMode: .fill)
                .clipped()
        }
    }
    
    private var cardContent: some View {
        VStack {
            VStack(alignment: .leading, spacing: 10) {
                Text(place.name)
                    .font(.system(size: 30, weight: .black))
                    .multilineTextAlignment(.leading)
                    .foregroundColor(place.photoUrls == nil ? .black : .white)
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                    .shadow(color: .black.opacity(0.5), radius: 10, y: 5)
            }
            .padding()
            .frame(
                maxWidth: isDetailed ? 0 : .infinity,
                alignment: .topLeading)
            
            HStack(spacing: 0) {
                VStack(alignment: .leading) {
                    if let rating = place.rating,
                        rating != 0 {
                        RatingBar(.stars(rating))
                            .frame(alignment: .trailing)
                        
                        Text("\(Int(rating)) звезды")
                            .foregroundColor(.white)
                            .font(.system(size: 16, weight: .bold))
                            .frame(alignment: .trailing)
                        
                    }
                }
                .frame(
                    maxWidth: .infinity,
                    alignment: .bottomLeading)
                if let telNumber = place.tel {
                    PhoneButton(phoneNumber: telNumber,
                                titleColor: place.photoUrls == nil ? .black : .white)
                }
            }
            .padding()
            .frame(maxWidth: .infinity,
                   alignment: .bottomLeading)
            .background(.ultraThinMaterial)
            .clipShape(.rect(cornerRadius: cornersRaduis))
            
        }
        .frame(
            maxHeight: .infinity,
            alignment: .bottom)
    }
}
