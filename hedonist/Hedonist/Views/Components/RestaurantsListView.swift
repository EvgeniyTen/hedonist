//
//  RestaurantsListView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 09.03.2024.
//

import SwiftUI

struct RestaurantsListView: View {
    @Binding var scrollDisabled: Bool
    @Binding var scrollIndex: Int
    var restaurants: [TripAdvisorRestaurantModel]
    var updateFavItem: (TripAdvisorRestaurantModel) -> Void
    
    init(
        scrollDisabled: Binding<Bool> = .constant(false),
        scrollIndex: Binding<Int>,
        restaurants: [TripAdvisorRestaurantModel],
        updateFavItem: @escaping (TripAdvisorRestaurantModel) -> Void
    ) {
        self._scrollDisabled = scrollDisabled
        self._scrollIndex = scrollIndex
        self.restaurants = restaurants
        self.updateFavItem = updateFavItem
    }
    
    var body: some View {
        ZStack {
            ScrollViewReader { proxy in
                ScrollView(.vertical, showsIndicators: false) {
                    LazyVStack {
                        ForEach(restaurants.indices, id: \.self) { index in
                            NavigationLink {
                                let place = restaurants[index]
                                DetailedCardView(
                                    place: place,
                                    action: {},
                                    favAction: updateFavItem
                                ) {
                                    RegularCardView(place: place, cornersRaduis: 0, isDetailed: true)
                                }
                            } label: {
                                cardView(index)
                            }
                        }
                    }
                }
                .scrollDisabled(scrollDisabled)
                .onChange(of: scrollIndex) { index in
                    withAnimation {
                        proxy.scrollTo(index, anchor: .top)
                    }
                }
            }
        }
    }
    
    func cardView(_ index: Int) -> some View {
        FavCardView(restaurants[index],
                    isSelected: index == scrollIndex)
            .padding(.top, index == 0 ? 15 : 0)
    }
}

#Preview {
    RestaurantsListView(scrollIndex: .constant(1), restaurants: [], updateFavItem: { _ in })
}
