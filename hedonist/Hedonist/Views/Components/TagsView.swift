//
//  TagsView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 08.05.2024.
//

import SwiftUI

struct TagView: View {
    let tags: [String]
    @State private var isExpanded: Bool = false
    private var contentCount: Int {
        isCountMoreThan5 ?
        isExpanded
        ? tags.count - 1
        : 5
        : tags.count - 1
    }
    
    private var isCountMoreThan5: Bool {
        tags.count >= 5
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            TagLayout {
                ForEach(0..<contentCount, id: \.self) { index in
                    Text(tags[index])
                        .font(.footnote)
                        .fontWeight(.ultraLight)
                        .foregroundColor(.black)
                        .padding(10)
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(Color("themeOrange"), lineWidth: 1)
                        }
                }
            }

            Button {
                isExpanded.toggle()
            } label: {
                Text(isExpanded ? "Показать меньше" : "Показать больше")
            }
            .isVisible(isCountMoreThan5)
        }
        .animation(.smooth, value: isExpanded)
    }

    func getImage(name: String?) -> Image? {
        guard let name else { return nil }
        return Image(name)
    }
}

struct TagLayout: Layout {
    var spacing: CGFloat = 5

    func sizeThatFits(
        proposal: ProposedViewSize,
        subviews: Subviews,
        cache: inout ()
    ) -> CGSize {
        let maxWidth: CGFloat = proposal.width ?? .zero
        var height: CGFloat = .zero
        let rows: [[LayoutSubviews.Element]] = generateRows(maxWidth,
                                                            proposal,
                                                            subviews)

        for (index, row) in rows.enumerated() {
            if index == (rows.count - 1) {
                height += row.maxHeight(proposal)
            } else {
                height += row.maxHeight(proposal) + spacing
            }
        }
        return .init(width: maxWidth, height: height)
    }

    func placeSubviews(
        in bounds: CGRect,
        proposal: ProposedViewSize,
        subviews: Subviews,
        cache: inout ()
    ) {
        var origin: CGPoint = bounds.origin
        let maxWidth: CGFloat = bounds.width
        let rows: [[LayoutSubviews.Element]] = generateRows(maxWidth,
                                                            proposal,
                                                            subviews)

        for row in rows {
            origin.x = bounds.origin.x
            for view in row {
                let viewSize: CGSize = view.sizeThatFits(proposal)
                view.place(at: origin, proposal: proposal)
                origin.x += (viewSize.width + spacing)
            }

            origin.y += (row.maxHeight(proposal) + spacing)
        }
    }

   private func generateRows(
        _ maxWidth: CGFloat,
        _ proposal: ProposedViewSize,
        _ subviews: Subviews
    ) -> [[LayoutSubviews.Element]] {
        var row: [LayoutSubviews.Element] = []
        var rows: [[LayoutSubviews.Element]] = []
        var origin: CGPoint = .zero

        for view in subviews.sorted(by: { $0.sizeThatFits(proposal).width < $1.sizeThatFits(proposal).width }) {
            let viewSize: CGSize = view.sizeThatFits(proposal)

            if (origin.x + viewSize.width + spacing) > maxWidth {
                rows.append(row)
                row.removeAll()
                origin.x = .zero
                row.append(view)
            } else {
                row.append(view)
            }
            origin.x += (viewSize.width + spacing)
        }

        if !row.isEmpty {
            rows.append(row)
            row.removeAll()
        }

        return rows
    }
}

extension [LayoutSubviews.Element] {
    func maxHeight(_ proposal: ProposedViewSize) -> CGFloat {
        self.compactMap { view in
            view.sizeThatFits(proposal).height
        }
        .max() ?? .zero
    }
}

#Preview {
    TagView(tags: [
        "Wi-fi",
        "Программа лояльности «Друзья ВДНХ»",
        "Туалеты",
        "Сервис аренды зарядок «Бери заряд»"
    ])
}
