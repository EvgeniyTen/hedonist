//
//  HedonistButtonView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 09.05.2023.
//

import SwiftUI

extension HedonistButtonView {
    class ViewModel: ObservableObject {
        let imageName: String?
        @Published var title: String
        let titleColor: Color
        let color: Color
        let height: CGFloat?
        var action: ()->Void
        
        init(imageName: String? = nil,
             title: String,
             titleColor: Color = .black,
             color: Color = .white,
             height: CGFloat? = nil,
             action: @escaping () -> Void) {
            self.imageName = imageName
            self.title = title
            self.titleColor = titleColor
            self.color = color
            self.height = height
            self.action = action
        }
    }
}

struct HedonistButtonView: View {
    
    @ObservedObject var viewModel: ViewModel
    @State var isTapped: Bool = false
    var body: some View {
        Button {
            isTapped = true
            let impactMed = UIImpactFeedbackGenerator(style: .medium)
            impactMed.impactOccurred()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                viewModel.action()
                isTapped = false
            }
        } label: {
            RoundedRectangle(cornerRadius: 20)
                .fill(viewModel.color)
                .frame(height: viewModel.height != nil ? viewModel.height : screenHeight(by: 0.075))
                .overlay(alignment: .center) {
                    if isTapped {
                        ProgressView()
                            .progressViewStyle(.circular)
                            .tint(.white)
                    } else {
                        HStack(spacing: 10) {
                            if let imageName = viewModel.imageName {
                                Image(systemName: imageName)
                                    .renderingMode(.template)
                                    .foregroundColor(.black)
                            }
                            Text(viewModel.title)
                                .foregroundColor(viewModel.titleColor)
                                .font(.system(size: 20, weight: .medium))
                        }
                    }
                }
        }
    }
}


struct HedonistButtonView_Previews: PreviewProvider {
    static var previews: some View {
        HedonistButtonView(viewModel: .init(imageName: "apple.logo", title: "Войти через Apple ID", action: {}))
    }
}
