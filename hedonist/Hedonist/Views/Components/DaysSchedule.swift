//
//  DaysSchedule.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 13.03.2024.
//

import SwiftUI

public struct DetailDay: Decodable, Hashable {
    public var day: Int
    public var start: String
    public var end: String
    public var workingHours: String {
        start + " - " + end
    }

    public init(day: Int, start: String, end: String) {
        self.day = day
        self.start = start
        self.end = end
    }

    public enum CodingKeys: String, CodingKey, CaseIterable {
        case day
        case start
        case end
    }
}


struct ScheduleView: View {
    let workingHours: [[String]?]
    var schedule: [DetailDay] {
        workingHours.enumerated().map { (offset, element) in
            DetailDay(day: offset, start: convertTimeFormat(element?[0]), end: convertTimeFormat(element?[1]))
        }
    }
    var body: some View {
        getSchedule(for: schedule)
    }
    
    func convertTimeFormat(_ timeString: String?) -> String {
        guard let timeString else { return "NAN"}
        let parts = timeString.split(separator: ":")
        
        switch parts.count {
        case 3: // Формат "08:00:00"
            let hoursAndMinutes = parts[0...1].joined(separator: ":")
            return hoursAndMinutes
        case 2: // Формат "08:00"
            return String(timeString)
        default:
            return "Неверный формат времени"
        }
    }
}

private extension ScheduleView {
    var weekdaySymbols: [String] {
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "ru_RU")
        return calendar.shortWeekdaySymbols
    }

    @ViewBuilder
    func getSchedule(for days: [DetailDay]) -> some View {
        VStack {
            let dict: [String: [DetailDay]] = days.groupBy { $0.workingHours }
            let workingDays: [Int] = days.map { $0.day }
            let nonWorkingDays: [Int] = getDaysOff(from: days)
            ForEach(dict.keys.sorted(), id: \.self) { day in
                workingDaysView(days: dict[day], workingHours: day, nonWorkingDays: nonWorkingDays)
            }

            daysOffView(days: nonWorkingDays, workingDays: workingDays)
        }
    }

    @ViewBuilder
    func workingDaysView(days: [DetailDay]?, workingHours: String, nonWorkingDays: [Int]) -> some View {
        if let days {
            HStack {
                let dayNumbers: [Int] = days.compactMap { $0.day }
                let daysSymbols: [String] = dayNumbers.compactMap { weekdaySymbols[$0] }
                let daysString: String = dayNumbers.compactMap { weekdaySymbols[$0] }.joined(separator: ", ")

                Group {
                    let isWorkingDaysHasGap: Bool = isDaysHasGap(workingDays: days.map { $0.day }, nonWorkingDays: nonWorkingDays)
                    switch days.count {
                    case 7:
                        Text("Ежедневно")
                    case _ where days.count > 2 && !isWorkingDaysHasGap:
                        Text("\(daysSymbols.first ?? "") — \(daysSymbols.last ?? "")")
                    default:
                        Text(daysString)
                    }
                }
                .foregroundStyle(.gray)
                .font(.system(size: 14, weight: .regular))

                Spacer()

                Text(workingHours)
                    .fontWeight(.bold)
                    .foregroundStyle(.black)
            }
        }
    }

    @ViewBuilder
    func daysOffView(days: [Int], workingDays: [Int]) -> some View {
        if !days.isEmpty {
            HStack {
                let daysSymbols: [String] = days.compactMap { weekdaySymbols[$0] }
                let daysString: String = days.compactMap { weekdaySymbols[$0] }.joined(separator: ", ")

                if days.count > 2 && !isDaysHasGap(workingDays: workingDays, nonWorkingDays: days) {
                    Text("\(daysSymbols.first ?? "") — \(daysSymbols.last ?? "")")
                        .foregroundStyle(.white)
                } else {
                    Text(daysString)
                        .foregroundStyle(.white)
                }

                Spacer()

                Text("Выходной")
                    .foregroundStyle(.white)
            }
            .padding(.top, 8)
        }
    }

    func isDaysHasGap(workingDays: [Int], nonWorkingDays: [Int]) -> Bool {
        guard !nonWorkingDays.isEmpty else { return false }

        for i in 0..<nonWorkingDays.count - 1 {
            let startNonWorkingDay: Int = nonWorkingDays[i]
            let endNonWorkingDay: Int = nonWorkingDays[i + 1]

            for workingDay in workingDays {
                if workingDay > startNonWorkingDay && workingDay < endNonWorkingDay {
                    return true
                }
            }
        }

        return false
    }

    func getDaysOff(from days: [DetailDay]) -> [Int] {
        let daysNumber: [Int] = days.map { $0.day }
        var daysOff: [Int] = []

        for day in 0...6 where !daysNumber.contains(day) {
            daysOff.append(day)
        }

        return daysOff
    }
}

#Preview {
    ZStack {
        Color.red
        ScheduleView(workingHours: TripAdvisorRestaurantModel.mock.workingHoursByDays ?? [])
    }
}
