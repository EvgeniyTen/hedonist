//
//  RestaurantAdressCart.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 16.04.2023.
//

import SwiftUI

extension RestaurantAdressCart {
    class ViewModel: ObservableObject {
        let textColor: Color
        let phoneNumber: String
        var latitude: Double?
        var longitude: Double?
        var address: String
        
        init(
            textColor: Color,
            phoneNumber: String,
            latitude: Double?,
            longitude: Double?,
            address: String
        ) {
            self.textColor = textColor
            self.phoneNumber = phoneNumber
            self.latitude = latitude
            self.longitude = longitude
            self.address = address
        }
    }
}

struct RestaurantAdressCart: View {
    @ObservedObject var viewModel: ViewModel
    @Binding var isUserInformed: Bool
    let action: () -> Void

    var body: some View {
        Button {
            showInformerBeforeRedirect()
        } label: {
            
            HStack() {
                Image(systemName: "location")
                    .renderingMode(.template)
                    .foregroundColor(.white)
                Text("Построить маршрут")
                    .foregroundColor(.white)
                    .lineLimit(2)
            }
            .padding()
            .frame(maxWidth: .infinity)
            .background {
                RoundedRectangle(cornerRadius: 5)
                    .foregroundColor(.black)
            }
            .shadow(radius: 5, y: 10)
        }
    }
    
    private func showInformerBeforeRedirect() {
        if isUserInformed {
            checkAndOpenURL()
        } else {
            action()
        }
    }
    
    private func checkAndOpenURL() {
        if let longitude = viewModel.longitude,
           let latitude = viewModel.latitude {
            let link: String = "https://yandex.ru/maps/213/moscow/?indoorLevel=1&ll=\(latitude)%2C\(longitude)&mode=routes&rtext=~\(latitude)%2C\(longitude)&rtt=auto&ruri=~&z=12"
            
            DispatchQueue.main.async {
                guard let url = URL(string: link), UIApplication.shared.canOpenURL(url) else {
                    return
                }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
}
struct RestaurantAdreessCart_Previews: PreviewProvider {
    static var previews: some View {
        RestaurantAdressCart(
            viewModel: .init(textColor: .white, phoneNumber: "", latitude: 0.0, longitude: 0.0, address: "Улица пушкина дом колтушкина"),
            isUserInformed: .constant(true)
        ) {}
    }
}
