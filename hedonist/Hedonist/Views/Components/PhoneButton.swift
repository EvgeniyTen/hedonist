//
//  PhoneButton.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 20.04.2023.
//

import SwiftUI

struct PhoneButton: View {
    var title: String = "Забронировать"
    let phoneNumber: String
    let titleColor: Color
    
    var body: some View {
        Button(action: {
            PhoneNumber(extractFrom: phoneNumber)?.makeACall()
        }, label: {
            Text(title)
                .font(.system(size: 16, weight: .medium))
                .foregroundColor(titleColor)
                .frame(maxWidth: .infinity)
                .padding()
                .overlay {
                    RoundedRectangle(cornerRadius: 5)
                        .stroke(titleColor)
                }
                .padding(1)
        })
    }
}

struct PhoneButton_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.white
            PhoneButton(phoneNumber: "+72389579234", titleColor: .black)
        }
    }
}
