//
//  NeighborhoodsListView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 14.03.2024.
//

import SwiftUI

extension NeighborhoodsListView {
    class ViewModel: ObservableObject {
        @Published var selectedHeighbor: String?
        @Published var sortedAndGroupedNeighborhoods: [(letter: String, neighborhoods: [String])] = []
        @Published var neighborhoods: [String] {
            willSet {
                sortedAndGroupedNeighborhoods = collectSections(newValue)
            }
        }
        @Published var searchResult: [String] = []
        @Published var searchText: String = "" {
            willSet {
                if newValue.isEmpty {
                    sortedAndGroupedNeighborhoods = collectSections(neighborhoods)
                } else {
                    sortedAndGroupedNeighborhoods = collectSections(neighborhoods.filter { $0.lowercased().contains(newValue.lowercased())})
                }
            }
        }
        
        func collectSections(_ newValue: [String]) -> [(letter: String, neighborhoods: [String])] {
            let uniqueNeighborhoods = Array(NSOrderedSet(array: newValue)) as! [String]
            let sortedNeighborhoods = uniqueNeighborhoods.sorted()
            let grouped = Dictionary(grouping: sortedNeighborhoods, by: { String($0.prefix(1)) })
            return grouped.map { ($0.key, $0.value) }.sorted { $0.letter < $1.letter }
        }
        
        var onTap: (String) -> Void

        init(
            selectedHeighbor: String? = nil,
            neighborhoods: [String],
            onTap: @escaping (String) -> Void
        ) {
            self.selectedHeighbor = selectedHeighbor
            self.neighborhoods = neighborhoods
            self.onTap = onTap
        }
    }
}

struct NeighborhoodsListView: View {
    @Environment(\.dismiss) var dismiss
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        NavigationView {
            VStack(spacing: .zero) {
                ScrollView(showsIndicators: false) {
                    ForEach(viewModel.sortedAndGroupedNeighborhoods, id: \.letter) { group in
                        Section(header: Text(group.letter).frame(maxWidth: .infinity, alignment: .leading)) {
                            ForEach(Array(group.neighborhoods.enumerated()), id: \.offset) { index, neighborhood in
                                Button {
                                    viewModel.selectedHeighbor = neighborhood
                                } label: {
                                    cell(title: neighborhood)
                                }
                            }
                        }
                    }
                }
                .padding(.horizontal, 25)
                
                HedonistButtonView(
                    viewModel: .init(
                        title: viewModel.selectedHeighbor == nil
                            ? "Закрыть"
                            : "Выбрать",
                        titleColor: .white,
                        color: viewModel.selectedHeighbor == nil
                            ? .gray
                            : Color(.themeOrange)
                    ) {
                        if let selectedHeighbor = viewModel.selectedHeighbor {
                            viewModel.onTap(selectedHeighbor)
                        }
                        dismiss()
                })
                .padding(.horizontal)
            }
        }
        .searchable(text: $viewModel.searchText)
    }
    
    func cell(title: String) -> some View {
        HStack {
            Image(systemName: "mappin.circle")
                .resizable()
                .frame(width: 30, height: 30)
            Text(title)
                .frame(maxWidth: .infinity, alignment: .leading)
        }
        .padding()
        .background(
            RoundedRectangle(cornerRadius: 25.0)
                .stroke(style: .init(lineWidth: 1))
                .padding(1)
        )
        .foregroundStyle(viewModel.selectedHeighbor == title ? Color(.themeOrange) : .black)
    }
}

#Preview {
    NeighborhoodsListView(viewModel: .init(neighborhoods: ["Qwerty", "Qwerty", "Qwerty"], onTap: { _ in }))
}
