//
//  MapView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 23.12.2023.
//

import SwiftUI
import MapKit

struct MapView<DetailView: View>: View {
    let isWide: Bool
    @State var isBlurred: Bool = false
    @State var selectedPointOfInterest: PointOfInterest?
    @State var region: MKCoordinateRegion
    let markers: [PointOfInterest]
    @ViewBuilder var detailViewProvider: (PointOfInterest?) -> DetailView
    
    init(
        isWide: Bool = false,
        latitude: Double? = 55.75222, // Moscow position by default
        longitude: Double? = 37.61556, // Moscow position by default
        markers: [PointOfInterest] = [],
        @ViewBuilder detailViewProvider: @escaping (PointOfInterest?) -> DetailView
    ) {
        self.isWide = isWide
        self.markers = markers
        self.detailViewProvider = detailViewProvider
        _region = State(initialValue: .init(center: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!),
                                             span: MKCoordinateSpan(
                                                latitudeDelta: isWide ? 0.5 : 0.05,
                                                longitudeDelta: isWide ? 0.5 : 0.05)))
    }
    
    var body: some View {
        ZStack(alignment: .center) {
            Map(coordinateRegion: $region,
                showsUserLocation: true,
                annotationItems: markers) { marker in
                MapAnnotation(coordinate: marker.coordinate) {
                    Button(action: {
                        isBlurred = true
                        selectedPointOfInterest = marker
                    }) {
                        Image(.mapPin)
                            .renderingMode(.original)
                    }
                }
            }
                .blur(radius: isBlurred ? 3.0 : 0)
            .edgesIgnoringSafeArea(.all)
            
            if let selectedPOI = selectedPointOfInterest {
                detailViewProvider(selectedPOI)
                    .transition(.opacity)
                    .padding(20)
                    .overlay(alignment: .topTrailing) {
                        Button {
                            isBlurred = false
                            selectedPointOfInterest = nil
                        } label: {
                            Image(systemName: "xmark")
                                .foregroundColor(.white)
                                .padding(5)
                                .background(
                                    Circle()
                                        .foregroundColor(.gray.opacity(0.5))
                                )
                        }
                    }
                    .padding(.horizontal)
            }
        }
        .animation(.easeInOut, value: selectedPointOfInterest)
    }
}

struct PointOfInterest: Identifiable, Equatable {
    let id = UUID()
    let title: String?
    let latitude: Double
    let longitude: Double
    
    init(title: String? = nil,
         latitude: Double?,
         longitude: Double?) {
        self.title = title
        self.latitude = latitude ?? 55.75222
        self.longitude = longitude ?? 37.61556
    }
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
