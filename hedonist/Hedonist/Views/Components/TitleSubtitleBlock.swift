//
//  TitleSubtitleBlock.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 10.05.2023.
//

import SwiftUI

extension TitleSubtitleBlock {
    class ViewModel: ObservableObject {
        let titleText: String
        let titleColor: Color?
        let titleImageName: String?
        let subtitleText: String
        let subTitleColor: Color?
        let subtitleImageName: String?

        init(titleText: String,
             titleColor: Color? = .black,
             titleImageName: String?,
             subtitleText: String,
             subTitleColor: Color? = .gray,
             subtitleImageName: String?) {
            self.titleText = titleText
            self.titleColor = titleColor
            self.titleImageName = titleImageName
            self.subtitleText = subtitleText
            self.subTitleColor = subTitleColor
            self.subtitleImageName = subtitleImageName
        }
    }
}

struct TitleSubtitleBlock: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                if let imageName =  viewModel.titleImageName {
                    Image(imageName)
                        .renderingMode(.template)
                        .foregroundColor(viewModel.titleColor)
                }
                Text(viewModel.titleText)
                    .foregroundColor(viewModel.titleColor)
                    .font(.system(size: 16, weight: .bold))
            }
            HStack {
                if let imageName =  viewModel.subtitleImageName {
                    Image(imageName)
                        .renderingMode(.template)
                        .foregroundColor(viewModel.subTitleColor)

                }
                Text(viewModel.subtitleText)
                    .foregroundColor(viewModel.subTitleColor)
                    .font(.system(size: 16, weight: .regular))
            }
        }
        .frame(
            maxWidth: .infinity,
            alignment: .leading)
    }
}

struct TitleSubtitleBlock_Previews: PreviewProvider {
    static var previews: some View {
        TitleSubtitleBlock(viewModel: .init(titleText: "Изучай новые заведения", titleColor: .black,  titleImageName: nil, subtitleText: "Список заведений актуален на текущий момент и будет периодически обновляться", subtitleImageName: nil))
    }
}
