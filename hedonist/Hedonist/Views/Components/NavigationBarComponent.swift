//
//  NavigationBarComponent.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 25.06.2023.
//

import SwiftUI

extension NavigationBarView {
    
    class ViewModel: ObservableObject {
        
        @Published var title: Title
        @Published var leftButtons: [BaseButtonViewModel]
        @Published var rightButtons: [BaseButtonViewModel]
        @Published var textColor: Color
        @Published var backgroundColor: Color
        @Published var state: State
        
        
        internal init(title: Title,
                      leftButtons: [BaseButtonViewModel] = [],
                      rightButtons: [BaseButtonViewModel] = [],
                      textColor: Color = .black,
                      state: State = .normal) {
            
            self.title = title
            self.leftButtons = leftButtons
            self.rightButtons = rightButtons
            self.textColor = textColor
            self.backgroundColor = .clear
            self.state = state
        }
        
        enum State {
            
            case normal
            case hidden
        }
        
        enum Title {
            
            case text(String)
            case empty
        }
        
        class BaseButtonViewModel: ObservableObject ,Identifiable {
            
            let id: UUID = UUID()
        }
        
        class ButtonViewModel: BaseButtonViewModel {
            
            @Published var icon: Image
            @Published var action: () -> Void
            
            init(icon: Image, action: @escaping () -> Void) {
                
                self.icon = icon
                self.action = action
                super.init()
            }
        }
        
        class TextButtonViewModel: BaseButtonViewModel {
            
            @Published var icon: Image?
            @Published var text: String?
            @Published var action: () -> Void
            
            init(
                icon: Image? = nil,
                text: String? = nil,
                action: @escaping () -> Void
            ) {
                self.icon = icon
                self.text = text
                self.action = action
                super.init()
            }
        }
        
        class BackButtonViewModel: BaseButtonViewModel {
            
            @Published var icon: Image
            @Published var action: () -> Void
            
            init(icon: Image = Image(.backIcon), action: @escaping () -> Void) {
                
                self.icon = icon
                self.action = action
                super.init()
            }
        }
    }
}

struct NavigationBarView: View {

    @ObservedObject var viewModel: ViewModel
    @Environment(\.dismiss) var dismiss
    
    var leftPlaceholdersCount: Int {
        return max(viewModel.rightButtons.count - viewModel.leftButtons.count, 0)
    }
    
    var rightPlaceholdersCount: Int {
        return max(viewModel.leftButtons.count - viewModel.rightButtons.count, 0)
    }
    
    var body: some View {
        ZStack {
            switch viewModel.state {
            case .normal:
                HStack(spacing: 0) {
                    HStack(alignment: .center, spacing: 18) {
                        ForEach(viewModel.leftButtons) { button in
                            
                            switch button {
                            case let backButtonViewModel as NavigationBarView.ViewModel.BackButtonViewModel:
                                Button {
                                    dismiss()
                                    backButtonViewModel.action()
                                } label: {
                                    Text("Назад")
                                        .font(.body)
                                        .lineLimit(1)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .foregroundColor(.black)
                                }
                                
                            case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                                Button(action: buttonViewModel.action){
                                    
                                    buttonViewModel.icon
                                        .resizable()
                                        .frame(width: 25, height: 25)
                                        .scaledToFit()
                                        .foregroundColor(.black)

                                }
                            default:
                                EmptyView()
                            }
                        }
                        
                        ForEach(0..<leftPlaceholdersCount, id: \.self) { _ in
                            Spacer().frame(width: 20, height: 20)
                        }
                    }
                    .frame(
                        maxWidth: .infinity,
                        alignment: .leading)
                                    
                    switch viewModel.title {

                    case .text(let title):
                        Text(title)
                            .font(.body)
                            .foregroundColor(viewModel.textColor)
                            .lineLimit(1)
                            .frame(maxWidth: .infinity, alignment: .center)
                        
                    case .empty:
                        EmptyView()
                    }
                                    
                    HStack(alignment: .center, spacing: 18) {
                        
                        ForEach(0..<rightPlaceholdersCount, id: \.self) { _ in
                            Spacer().frame(width: 20, height: 20)
                        }
                        
                        ForEach(viewModel.rightButtons) { button in
                            
                            switch button {
                            case let buttonViewModel as NavigationBarView.ViewModel.ButtonViewModel:
                                Button(action: buttonViewModel.action){
                                    
                                    buttonViewModel.icon
                                        .resizable()
                                        .frame(width: 25, height: 25)
                                        .scaledToFit()
                                        .foregroundColor(.black)

                                }
                                
                            case let buttonViewModel as NavigationBarView.ViewModel.TextButtonViewModel:
                                Button(action: buttonViewModel.action){
                                    HStack {
                                        if let icon = buttonViewModel.icon {
                                            icon
                                                .resizable()
                                                .frame(width: 25, height: 25)
                                                .scaledToFit()
                                                .foregroundColor(.black)
                                        }
                                        if let title = buttonViewModel.text {
                                            Text(title)
                                                .font(.body)
                                                .foregroundColor(viewModel.textColor)
                                                .lineLimit(1)
                                        }
                                    }

                                }
                            default:
                                EmptyView()
                            }
                        }
                    }
                    .frame(
                        maxWidth: .infinity,
                        alignment: .trailing)
                }
                .frame(height: 48)
                .background(viewModel.backgroundColor.edgesIgnoringSafeArea(.top))
                
            case .hidden:
                EmptyView()
            }
        }
        .padding(.horizontal)
    }

}

struct NavigationBarViewModifier: ViewModifier {
    
    @State var viewModel: NavigationBarView.ViewModel
    
    func body(content: Content) -> some View {
        
        switch viewModel.state {
            
        case .normal:
            
            VStack(spacing: 0) {
                
                NavigationBarView(viewModel: viewModel)
                content
            }
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden()
            
        case .hidden:
            content
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden()
        }
    }
}

extension View {
    
    func navigationBar(with viewModel: NavigationBarView.ViewModel) -> some View {
        
        self.modifier(NavigationBarViewModifier(viewModel: viewModel))
    }
}

extension NavigationBarView.ViewModel {
    
    static let sample0 = NavigationBarView.ViewModel(
        title: .empty,
        rightButtons: [
            NavigationBarView.ViewModel.ButtonViewModel(icon: Image(systemName: "heart.fill"), action: {})
        ]
    )
}

struct NavigationBarView_Previews: PreviewProvider {
    
    static var previews: some View {

            ScrollView {
                VStack(spacing: 10) {
                    Color.green
                        .frame(height: 180)
                    Color.orange
                        .frame(height: 180)
                }
            }
            .navigationBar(with: .sample0)
    }
}
