//
//  RatingBar.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 16.04.2023.
//

import SwiftUI

enum RatingType {
    case stars(Double)
    case price(Double)
}

struct RatingBar: View {
    private static let maxRating: Double = 5
    
    let rating: Double
    private let fullStarImage: String
    private let halfFullStarImage: String
    private let emptyStarImage: String
    private let color: Color
    private let fullCount: Int
    private let emptyCount: Int
    private let halfFullCount: Int
    
    init(_ ratingType: RatingType) {
        switch ratingType {
        case .stars(let rating):
            self.rating = rating
            self.fullStarImage = "star.fill"
            self.halfFullStarImage = "star.lefthalf.fill"
            self.emptyStarImage = "star"
            self.color = .orange
        case .price(let rating):
            self.rating = rating
            self.fullStarImage = "rublesign.circle.fill"
            self.halfFullStarImage = "rublesign.circle"
            self.emptyStarImage = "rublesign.circle"
            self.color = .gray
            
        }
        fullCount = Int(rating)
        emptyCount = Int(RatingBar.maxRating - rating)
        halfFullCount = (Double(fullCount + emptyCount) < RatingBar.maxRating) ? 1 : 0
    }
    
    var body: some View {
        HStack(spacing: 5) {
            ForEach(0..<fullCount, id: \.self) { _ in
                self.fullStar
            }
            ForEach(0..<halfFullCount, id: \.self) { _ in
                self.halfFullStar
            }
            ForEach(0..<emptyCount, id: \.self) { _ in
                self.emptyStar
            }
        }
        .animation(.easeInOut, value: rating)
    }
    
    private var fullStar: some View {
        Image(systemName: fullStarImage)
            .renderingMode(.template)
            .resizable()
            .foregroundColor(color)
            .frame(width: 15, height: 15)
    }
    
    private var halfFullStar: some View {
        Image(systemName: halfFullStarImage)
            .renderingMode(.template)
            .resizable()
            .foregroundColor(color)
            .frame(width: 15, height: 15)
    }
    
    private var emptyStar: some View {
        Image(systemName: emptyStarImage)
            .renderingMode(.template)
            .resizable()
            .foregroundColor(color)
            .frame(width: 15, height: 15)
    }
}

struct StarsBar_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            RatingBar(.price(4.5))
            RatingBar(.stars(4.5))
        }
    }
}
