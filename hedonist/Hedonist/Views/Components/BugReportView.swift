//
//  BugReportView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 17.03.2024.
//

import FirebaseFirestore
import FirebaseStorage
import SwiftUI

extension BugReportView {
    func sendReport(reportText: String, images: [UIImage]) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        ref = db.collection("reviews").addDocument(data: [
            "description": reportText,
            "timestamp": Timestamp(date: Date())
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                self.uploadImages(images: images, reportId: ref!.documentID)
            }
        }
    }
    
    func uploadImages(images: [UIImage], reportId: String) {
        let storage = Storage.storage()
        for image in images {
            if let imageData = image.jpegData(compressionQuality: 0.8) {
                let uuid = UUID().uuidString
                let storageRef = storage.reference().child("bugReports/media/\(uuid).jpg")
                storageRef.putData(imageData, metadata: nil) { (metadata, error) in
                    if let error = error {
                        print("Error uploading image: \(error)")
                        return
                    }
                    print("Image uploaded successfully")
                }
            }
        }
    }
}

struct BugReportView: View {
    @Environment(\.dismiss) var dismiss
    @State private var reportText = ""
    @State private var showingImagePicker = false
    @State private var images: [UIImage] = []
    
    var body: some View {
        Form {
            Section(header: Text("Описание проблемы")) {
                TextEditor(text: $reportText)
                    .frame(height: 200)
            }
            
            Section {
                Button(action: {
                    self.showingImagePicker = true
                }) {
                    Text("Добавить фото или видео")
                }
            }
            
            if !images.isEmpty {
                Section(header: Text("Прикрепленные фото")) {
                    ForEach(images, id: \.self) { img in
                        Image(uiImage: img)
                            .resizable()
                            .scaledToFit()
                    }
                }
            }
            
            Section {
                Button("Отправить") {
                    if !reportText.isEmpty, !images.isEmpty {
                        sendReport(reportText: reportText, images: images)
                        dismiss()
                    }
                }
            }
        }
        .sheet(isPresented: $showingImagePicker) {
            ImagePicker(images: $images, isShown: $showingImagePicker)
                .edgesIgnoringSafeArea(.bottom)
        }
        .navigationBar(with: .init(title: .text("Обратная связь"), leftButtons: [NavigationBarView.ViewModel.BackButtonViewModel(action: {dismiss()})]))
    }
}

struct ImagePicker: UIViewControllerRepresentable {
    @Binding var images: [UIImage]
    @Binding var isShown: Bool

    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }

    func makeUIViewController(context: Context) -> some UIViewController {
        var picker = UIImagePickerController()
        picker.delegate = context.coordinator
        return picker
    }

    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {}

    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        var parent: ImagePicker

        init(_ parent: ImagePicker) {
            self.parent = parent
        }

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[.originalImage] as? UIImage {
                parent.images.append(image)
            }

            parent.isShown = false
        }
    }
}

#Preview {
    BugReportView()
}
