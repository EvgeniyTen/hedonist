//
//  AnimatableGradientView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 20.04.2023.
//

import SwiftUI
import Combine

struct AnimatableGradientView: View {
    
    let timer = Timer.publish(every: 4, on: .current, in: .common).autoconnect()
    let action: ()-> Void
    @State private var gradientA: [Color] = [.green, .black]
    @State private var gradientB: [Color] = [.white, .blue]
    
    @State private var firstPlane: Bool = true
    
    func setGradient(gradient: [Color]) {
        if firstPlane {
            gradientB = gradient
        }
        else {
            gradientA = gradient
        }
        firstPlane = !firstPlane
    }
    
    var body: some View {
        ZStack {
            Rectangle()
                .fill(LinearGradient(gradient: Gradient(colors: self.gradientA), startPoint: UnitPoint(x: 0, y: 0), endPoint: UnitPoint(x: 1, y: 1)))
            Rectangle()
                .fill(LinearGradient(gradient: Gradient(colors: self.gradientB), startPoint: UnitPoint(x: 0, y: 0), endPoint: UnitPoint(x: 1, y: 1)))
                .opacity(self.firstPlane ? 0 : 1)
        }
        .onReceive(timer) { _ in
            withAnimation(.spring(response: 1.5,blendDuration: 3)) {
                self.setGradient(gradient: [Color.random(), Color.random()])
            }
        }
        .onTapGesture {
            withAnimation(.spring(response: 1.5,blendDuration: 3)) {
                self.setGradient(gradient: [Color.random(), Color.random()])
            }
            action()
        }
    }
}

struct AnimatableGradientView_Previews: PreviewProvider {
    static var previews: some View {
        AnimatableGradientView(action: {})
    }
}
