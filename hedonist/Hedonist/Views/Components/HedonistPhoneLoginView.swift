//
//  HedonistPhoneLoginView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 25.07.2023.
//

import SwiftUI

extension HedonistPhoneLoginView {
    class ViewModel: ObservableObject {
        
        private let core: Core

        @Published var textFieldTransition: Bool = false
        @Published var showInputVerificationTF: Bool = false
        @Published var isViewShaked: Bool = false
        
        @Published var userCountryCode: String = "+7 "
        @Published var userPhoneNumber: String = ""
        @Published var userName: String = ""
        @Published var verificationCode: String = ""

        @Published var loginButton: HedonistButtonView.ViewModel
        @Published var phoneButton: HedonistButtonView.ViewModel
        
        @Published var successDismiss: () -> Void
        @Published var dismiss: () -> Void
        
        init(core: Core,
             successDismiss: @escaping() -> Void,
             dismiss: @escaping() -> Void) {
            self.core = core
            self.successDismiss = successDismiss
            self.dismiss = dismiss
            
            self.loginButton = .init(title: "Войти",titleColor: .white, color: .gray, action: {})
            self.phoneButton = .init(title: "Далее",titleColor: .white, color: .gray, action: {})

            self.loginButton.action = loginAction
            self.phoneButton.action = phoneAuthTapped

        }
        
        func loginAction() {
//            self.core.loginManager.phoneCodeVerifyHandler(verificationCode) { [weak self] success in
//                if success {
//                    self?.successDismiss()
//                } else {
//                    self?.isViewShaked.toggle()
//                    DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.15) {
//                        self?.isViewShaked.toggle()
//                        self?.dismiss()
//                    }
//                }
//            }
        }
        
        private func phoneAuthTapped() {
            if userName == "" {
                self.isViewShaked.toggle()
                DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.15) {
                    self.isViewShaked.toggle()
                }
                return
            } else if userName != "", textFieldTransition, showInputVerificationTF {
                if textFieldTransition {
                    showInputVerificationTF = true
                } else {
                    textFieldTransition = true
                }
                return
            } else if textFieldTransition, userPhoneNumber == "" {
                self.isViewShaked.toggle()
                DispatchQueue.main.asyncAfter(wallDeadline: .now() + 0.15) {
                    self.isViewShaked.toggle()
                }
                return
            } else {
                if userName != "", textFieldTransition, showInputVerificationTF {
                } else if textFieldTransition {
//                    core.loginManager.phoneLoginRequest(name: userName, phoneNumber:userCountryCode + userPhoneNumber) { [weak self] success in
//                        self?.showInputVerificationTF = true
//                    }
                } else {
                    textFieldTransition = true
                }
            }
        }
    }
}

struct HedonistPhoneLoginView: View {
    
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack(spacing: 15) {
            HStack {
                Spacer()
                Button {
                    viewModel.dismiss()
                } label: {
                    Image(systemName: "xmark")
                        .foregroundColor(.white)
                        .padding(5)
                        .background(
                            Circle()
                                .foregroundColor(.gray.opacity(0.5))
                        )
                }

            }
            ZStack {
                if !viewModel.userName.isEmpty ,viewModel.textFieldTransition, !viewModel.showInputVerificationTF {
                    HStack {
                        Picker(selection: .constant(1), label: Text(viewModel.userCountryCode)) {
                            Text("+7 ")
                                .tag(1)
                                
                            Text("+8 ")
                                .tag(2)
                                
                        }
                        
                        TextField("123 456-78-90", text: $viewModel.userPhoneNumber)
                            .keyboardType(.numberPad)
                            .padding()
                            .foregroundColor(.black)
                            .frame(height: screenHeight(by: 0.06))
                            .background {
                                RoundedRectangle(cornerRadius: 20)
                                    .foregroundColor(.white)
                            }
                            .transition(.asymmetric(insertion: .move(edge: .trailing),removal: .move(edge: .leading)))
                            .shadow(color: .gray.opacity(0.5), radius: 5)
                    }
                }
                if viewModel.showInputVerificationTF {
                    TextField("Введите код здесь", text: $viewModel.verificationCode)
                        .keyboardType(.numberPad)
                        .padding()
                        .foregroundColor(.black)
                        .frame(height: screenHeight(by: 0.06))
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .foregroundColor(.white)
                        }
                        .transition(.asymmetric(insertion: .move(edge: .trailing),removal: .move(edge: .leading)))
                        .shadow(color: .gray.opacity(0.5), radius: 5)
                        
                }
                if !viewModel.textFieldTransition {
                    TextField("Введите Ваше имя", text: $viewModel.userName)
                        .padding()
                        .foregroundColor(.black)
                        .textInputAutocapitalization(.sentences)
                        .frame(height: screenHeight(by: 0.06))
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .foregroundColor(.white)
                        }
                        .transition(.asymmetric(insertion: .move(edge: .trailing),removal: .move(edge: .leading)))
                        .shadow(color: .gray.opacity(0.5), radius: 5)
                }
            }
            .offset(x: viewModel.isViewShaked ? -10 : 0)
            .animation(Animation.default.repeatCount(2).speed(5), value: viewModel.isViewShaked)
            
            if viewModel.showInputVerificationTF {
                HedonistButtonView(viewModel: viewModel.loginButton)
                    .frame(height: screenHeight(by: 0.06))
                    .opacity(0.7)
                
            } else {
                HedonistButtonView(viewModel: viewModel.phoneButton)
                    .frame(height: screenHeight(by: 0.06))
                    .cornerRadius(20)
            }
        }
    }
}

struct HedonistPhoneLoginView_Previews: PreviewProvider {
    static var previews: some View {
        HedonistPhoneLoginView(viewModel: .init(core: .emptyMock, successDismiss: {}, dismiss: {}))
    }
}
