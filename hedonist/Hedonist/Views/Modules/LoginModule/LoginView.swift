//
//  LoginView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 09.05.2023.
//

import SwiftUI
import AuthenticationServices

struct LoginView: View {
    
    @ObservedObject var viewModel: LoginViewModel
    @AppStorage("selectedAppearance") var selectedAppearance = 1

    @State var hiddenTaps = false
    var body: some View {
        
        ZStack{
            Image("startScreen")
                .resizable()
                .imageScale(.large)
                .ignoresSafeArea()
                .blur(radius: viewModel.showPhoneLogin ? 15 : 0, opaque: true)
            
            VStack {
                titleWithSecret
                
                LoginTypeSelectionView(viewModel: viewModel.loginTypeSelectionViewModel)
                    .blur(radius: viewModel.showPhoneLogin ? 15 : 0, opaque: false)
            }

            // MARK: Nav links
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                if let link = viewModel.link  {
                    switch link {
                    case .descriptionView(let viewModel):
                        DescriptionView(viewModel: viewModel)
                        
                    case .appleIDTapped(let viewModel):
                        DescriptionView(viewModel: viewModel)
                    }
                }
            }
        }
        .onAppear{
            selectedAppearance = 0
        }
        .onChange(of: selectedAppearance, perform: { value in
                viewModel.utilities.overrideDisplayMode(value)
                })
        .alert(isPresented: $viewModel.showErrorAlert) {
            Alert(title: Text("не разработано"), dismissButton: .cancel(Text("Ok")) {
                viewModel.showErrorAlert = false
            })
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(viewModel: .init(core: .emptyMock))
    }
}

extension LoginView {
    var titleWithSecret: some View {
        ZStack {
            Image("logoTitle")
                .renderingMode(.original)
                .frame(height: screenHeight(by: 0.1), alignment: .top)
                .frame(maxHeight: .infinity, alignment: .bottom)

        }
    }
}
