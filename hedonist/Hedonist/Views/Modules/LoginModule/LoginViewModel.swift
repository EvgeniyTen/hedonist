//
//  LoginViewModel.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 09.05.2023.
//

import Foundation
import Combine
import AuthenticationServices


class LoginViewModel: ObservableObject {
    
    //appearing theme configurator
    var utilities = Utilities()
    
    private let core: Core
    private let action: PassthroughSubject<Action, Never> = .init()
    private var dispose = Set<AnyCancellable>()
    
    
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    
    @Published var isLinkActive: Bool = false
    @Published var showErrorAlert: Bool = false
    @Published var showPhoneLogin: Bool = false
    @Published var loginTypeSelectionViewModel: LoginTypeSelectionView.ViewModel
    
    init(core: Core,
         loginTypeSelectionViewModel: LoginTypeSelectionView.ViewModel
    ) {
        self.core = core
        self.loginTypeSelectionViewModel = loginTypeSelectionViewModel
    }
    
    convenience init(core: Core) {
        self.init(core: core,
                  loginTypeSelectionViewModel: .init(
                    core: core,
                    successDismiss: {}))
        loginTypeSelectionViewModel.successDismiss = showNextScreen
        bind()
    }
    
    func bind() {
        core.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                case _ as LoginViewAction.LoginTapped:
                    showNextScreen()
                default: break
                }
            }
            .store(in: &dispose)
    }
    
    func showNextScreen() {
        link = .descriptionView(DescriptionViewModel(core: core))
    }
    enum Link {
        case appleIDTapped(DescriptionViewModel)
        case descriptionView(DescriptionViewModel)
    }
    
}

enum LoginViewAction {
    struct LoginTapped: Action {}
}
