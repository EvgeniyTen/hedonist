//
//  DescriptionView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 10.05.2023.
//

import SwiftUI

struct DescriptionView: View {
    
    @AppStorage("selectedAppearance") var selectedAppearance = 0

    
    @ObservedObject var viewModel: DescriptionViewModel
    
    var body: some View {
        ZStack {
            Color.white.ignoresSafeArea()
            OnboardingImagesBlock()
                .gradientForeground(colors: [.white.opacity(0.5), .clear])
                .modifier(ParallaxMotionModifier(magnitude: 10))
                .position(x: 205, y: screenHeight(by: 0.25))
                .clipped()
                .ignoresSafeArea()
            VStack(alignment: .leading) {
                Text("Рандомайзер\nпо ресторанам")
                    .font(.system(size: 27, weight: .bold))
                    .foregroundColor(.black)
                    .frame(alignment: .leading)
                    .padding(.bottom)

                if let top = viewModel.topTitlesBlock,
                   let middle = viewModel.middleTitlesBlock,
                   let bottom = viewModel.bottomTitlesBlock {
                    TitleSubtitleBlock(viewModel: top)
                    TitleSubtitleBlock(viewModel: middle)
                    TitleSubtitleBlock(viewModel: bottom)
                }
                
                if let button = viewModel.nextButton {
                    HedonistButtonView(viewModel: button)
                        .padding(.top)
                }
            }
            .frame(maxHeight: .infinity, alignment: .bottom)
            .padding(.horizontal)
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                if let link = viewModel.link  {
                    switch link {
                    case .filter(let viewModel):
                        FilterView(viewModel: viewModel)
                            .transition(.opacity)
                        
                    case .location(let viewModel):
                        LocationReqestView(viewModel: viewModel)
                            .transition(.opacity)
                    }
                }
            }
        }
        .onAppear{
            selectedAppearance = 1
        }
        .onChange(of: selectedAppearance, perform: { value in
                viewModel.utilities.overrideDisplayMode(value)
                })
        .navigationBarBackButtonHidden()
    }
}

struct DescriptionView_Previews: PreviewProvider {
    static var previews: some View {
        DescriptionView(viewModel: DescriptionViewModel(core: .emptyMock))
    }
}

