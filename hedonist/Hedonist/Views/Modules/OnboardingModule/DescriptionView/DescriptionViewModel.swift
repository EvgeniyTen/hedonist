//
//  OnboardingViewModel.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 10.05.2023.
//

import SwiftUI
import Combine

class DescriptionViewModel: ObservableObject {
    private let core: Core
    var utilities = Utilities()

    private var dispose = Set<AnyCancellable>()

    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    @Published var topTitlesBlock: TitleSubtitleBlock.ViewModel?
    @Published var middleTitlesBlock: TitleSubtitleBlock.ViewModel?
    @Published var bottomTitlesBlock: TitleSubtitleBlock.ViewModel?
    @Published var nextButton: HedonistButtonView.ViewModel?

    init(core: Core,
         topTitlesBlock: TitleSubtitleBlock.ViewModel?,
         middleTitlesBlock: TitleSubtitleBlock.ViewModel?,
         bottomTitlesBlock: TitleSubtitleBlock.ViewModel?,
         nextButton: HedonistButtonView.ViewModel?) {
        self.core = core
        self.topTitlesBlock = topTitlesBlock
        self.middleTitlesBlock = middleTitlesBlock
        self.bottomTitlesBlock = bottomTitlesBlock
        self.nextButton = nextButton
    }
    
    convenience init(core: Core) {
        self.init(core: core,
                  topTitlesBlock: nil,
                  middleTitlesBlock: nil,
                  bottomTitlesBlock: nil,
                  nextButton: nil)
        
        self.topTitlesBlock = .init(
            titleText: "Изучай новые заведения",
            titleImageName: "cutlery",
            subtitleText: "Список заведений актуален на текущий момент и будет периодически обновляться",
            subtitleImageName: nil)
        
        self.middleTitlesBlock = .init(
            titleText: "Подбери случайное место",
            titleImageName: "dices",
            subtitleText: "Настраивай фильтры по своим интересам для идеального совпадения",
            subtitleImageName: nil)
        
        self.bottomTitlesBlock = .init(
            titleText: "Сохраняй места в избранное",
            titleImageName: "heart",
            subtitleText: "Храни список своих любимых заведений в одном месте",
            subtitleImageName: nil)
        
        self.nextButton = .init(
            title: "Далее",
            titleColor: .white,
            color: Color("themeOrange"),
            action: tapAction)

    }

    private func tapAction() {
//        link = .filter(.init(core: core))
        link = .location(.init(core: core))

    }
    
    enum Link {
        case location(LocationReqestViewModel)
        case filter(FilterViewModel)
    }
}
