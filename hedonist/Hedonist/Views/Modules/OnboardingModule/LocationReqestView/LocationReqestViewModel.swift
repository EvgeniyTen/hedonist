//
//  LocationReqestViewModel.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 23.05.2023.
//

import Foundation
import Combine
import AuthenticationServices
import SwiftUI


class LocationReqestViewModel: ObservableObject {
    
    private let core: Core
    private var dispose = Set<AnyCancellable>()
    
    var utilities = Utilities()

    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var isLinkActive: Bool = false
    @Published var neighborhood: String?
    @Published var isNeighborNotDetected: Bool = true
    
    @Published var descriptionBlock: TitleSubtitleBlock.ViewModel?
    @Published var formattedAddress: TitleSubtitleBlock.ViewModel?
    @Published var nextButton: HedonistButtonView.ViewModel?
    
    init(core: Core,
         login: HedonistButtonView.ViewModel?,
         descriptionBlock: TitleSubtitleBlock.ViewModel?) {
        self.core = core
        self.nextButton = nextButton
        self.descriptionBlock = descriptionBlock
        self.core.askLocationPermission()
        self.bind()
    }
    
    convenience init(core: Core) {
        self.init(core: core,
                  login: nil,
                  descriptionBlock: nil)
        
        self.nextButton = .init(
            title: "Далее",
            titleColor: .white,
            color: Color("themeOrange"),
            action: nextTapped)
        
        self.descriptionBlock = .init(
            titleText: "Для самых точных рассчетов.",
            titleColor: .white,
            titleImageName: nil,
            subtitleText: "Нам потребуется информация о твоей локации для сбора ресторанов рядом с тобой.",
            subtitleImageName: nil)
    }

    private func bind() {
        core.currentAdress
            .combineLatest(core.locationManager.status)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] adress, status in
                if status == .disabled {
                    formattedAddress = .init(titleText: "Ничего страшного", titleImageName: nil, subtitleText: "🥲 Для определения геопозиции можно будет воспользоваться списком предусмотренных нами локаций", subtitleImageName: nil)
                }
                if let neighbor = adress?.neighbor {
                    neighborhood = neighbor
                }
                if let fullAddress = adress?.adress,
                   let neighbor = adress?.neighbor {
                    formattedAddress = .init(
                        titleText: "Ваш адрес:",
                        titleColor: .white,
                        titleImageName: nil,
                        subtitleText: fullAddress,
                        subtitleImageName: nil
                    )
                }
            }
            .store(in: &dispose)
        
        core.restaurantProperties
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] properties in
                if let neighborhoodProperties = properties["neighborhood"] as? [String],
                   let neighborhood,
                   let _ = neighborhoodProperties.first(where: {$0.lowercased().contains(neighborhood)}) {
                    isNeighborNotDetected = false
                }
            }
            .store(in: &dispose)
    }
    private func nextTapped() {
        link = .filter(.init(core: core))

    }
    
    enum Link {
        case filter(FilterViewModel)
    }
}

enum LocationReqestViewAction {
    struct NextTapped: Action {}
}
