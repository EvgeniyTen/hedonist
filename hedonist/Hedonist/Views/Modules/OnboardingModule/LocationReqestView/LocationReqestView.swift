//
//  LocationReqestView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 23.05.2023.
//

import SwiftUI

struct LocationReqestView: View {
    
    
    @ObservedObject var viewModel: LocationReqestViewModel
    @AppStorage("selectedAppearance") var selectedAppearance = 1

    
    var body: some View {
        ZStack {
            Color.black.ignoresSafeArea()
            Image("circlePattern")
                .offset(x: -230, y: -190)
            Image("circlePattern")
                .offset(x: 140)
            
            VStack {
                VStack(spacing: 50) {
                    Image("locationMainImage")
                        .frame(maxHeight: .infinity)
                    VStack {
                        if let neighborhood = viewModel.neighborhood {
                            Text("Ваш район:")
                                .fontWeight(.bold)
                                .foregroundStyle(.gray)
                                .frame(maxWidth: .infinity, alignment: .leading)
                            
                            Text(neighborhood)
                                .font(.system(size: 24, weight: .black))
                                .foregroundStyle(.white)
                                .frame(maxWidth: .infinity, alignment: .leading)
                            
                            if viewModel.isNeighborNotDetected {
                                Text("Этот район не был нами определен")
                                    .multilineTextAlignment(.leading)
                                    .foregroundStyle(.red)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                Text("Вы можете выбрать интересующий Вас район на следующем экране")
                                    .multilineTextAlignment(.leading)
                                    .foregroundStyle(.gray)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            }
                        }
                        
                    }
                    if let formattedAddress = viewModel.formattedAddress {
                        TitleSubtitleBlock(viewModel: formattedAddress)
                    } else {
                        LoaderView()
                    }
                    
                    if let description = viewModel.descriptionBlock {
                        TitleSubtitleBlock(viewModel: description)
                    }
                }
            }
            .padding(.horizontal)
            .navigationBarBackButtonHidden()
            
            

            NavigationLink("", isActive: $viewModel.isLinkActive) {
                if let link = viewModel.link  {
                    switch link {
                    case .filter(let viewModel):
                        FilterView(viewModel: viewModel)
                            .transition(.opacity)
                    }
                }
            }
        }
        .onAppear{
            selectedAppearance = 0
        }
        .onChange(of: selectedAppearance) { _, newValue in
            viewModel.utilities.overrideDisplayMode(newValue)
        }
    }
}

struct LocationReqestView_Previews: PreviewProvider {
    static var previews: some View {
        LocationReqestView(viewModel: .init(core: .emptyMock))
    }
}
