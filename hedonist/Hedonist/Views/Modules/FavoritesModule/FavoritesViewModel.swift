//
//  FavoritesViewModel.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 23.12.2023.
//

import SwiftUI
import Combine
import Accelerate

class FavoritesViewModel: ObservableObject {
    private let core: Core
    private var dispose = Set<AnyCancellable>()
    
    @Published var area: (Double, Double) = (0, 0)
    @Published var favPlaces: [TripAdvisorRestaurantModel] = []
    @Published var mapMarkers: [PointOfInterest] = []
    @Published var index: Int? {
        willSet {
            if newValue != nil {
                isDetailCardVisible = true
            } else {
                isDetailCardVisible = false
            }
        }
    }
    @Published var navigationBar: NavigationBarView.ViewModel
    @Published var isDetailCardVisible: Bool = false

    init(core: Core) {
        self.core = core
        self.navigationBar = .init(
            title: .empty,
            leftButtons: [NavigationBarView.ViewModel.BackButtonViewModel(action: {})])
        bind()
    }
    
    func bind() {
        core.favoriteRestaurants
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] places in
                favPlaces = places
                let points = places.compactMap { ($0.name, $0.latitude, $0.longitude) }
                mapMarkers = points.map { PointOfInterest(title: $0.0, latitude: $0.1, longitude: $0.2) }
                generateArea(with: places)
            }
            .store(in: &dispose)
    }
    
    func showDetail(with value: Int? ) {
        withAnimation(.interactiveSpring(response: 0.6, dampingFraction: 0.7, blendDuration: 0.7)) {
            index = value
        }
    }
    
    func updateFavItem(item: TripAdvisorRestaurantModel) {
        core.action.send(CoreModelAction.RestaurantManagerAction.UpdateFavItem(value: item))
    }
    
    private func generateArea(with places: [TripAdvisorRestaurantModel]) {
        area = (
            vDSP.mean(places.compactMap { $0.latitude}),
            vDSP.mean(places.compactMap { $0.longitude})
        )

    }
}
