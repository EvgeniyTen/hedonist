//
//  FavoritesView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 23.12.2023.
//

import SwiftUI
import Kingfisher

struct FavoritesView: View {
    
    @ObservedObject var viewModel: FavoritesViewModel
    @State var scrollIndex: Int = 0

    @Namespace var animation
    var body: some View {
        ZStack {
            if viewModel.favPlaces.isEmpty {
                VStack(alignment: .center, spacing: 20) {
                    Image(.favsEmpty)
                        .frame(maxHeight: screenHeight(by: 0.4), alignment: .bottom)
                    Text("Тут пока ничего нет")
                        .font(.system(size: 18, weight: .bold))
                        .foregroundStyle(.white)
                        .padding(.horizontal)
                        .padding(.vertical, 5)
                        .background(Color("themeOrange"))
                        .clipShape(.rect(cornerRadius: 20))
                    Text("Жми сердечко на местах,\nкоторые тебе нравятся!")
                        .foregroundStyle(.gray)
                        .font(.system(size: 15, weight: .regular))
                }
                .frame(maxHeight: .infinity)

            } else {
                MapView(
                    isWide: true,
                    latitude: viewModel.area.0,
                    longitude: viewModel.area.1,
                    markers: viewModel.mapMarkers) { point in
                        if let place = viewModel.favPlaces.first(where: { $0.name == point?.title }) {
                            NavigationLink {
                                DetailedCardView(
                                    place: place,
                                    action: {},
                                    favAction: viewModel.updateFavItem
                                ) {
                                    RegularCardView(place: place, cornersRaduis: 0, isDetailed: true)
                                }
                            } label: {
                                FavCardView(place, isSelected: true)
                            }
                        }
                    }
                .ignoresSafeArea()
            }
        }
        .navigationBar(with: viewModel.navigationBar)
    }
    
    func selectPlace(_ place: PointOfInterest) {
        scrollIndex = viewModel.favPlaces.firstIndex(where: { $0.name == place.title }) ?? 0
    }
}

#Preview {
    FavoritesView(viewModel: .init(core: .emptyMock))
}

struct FavCardView: View {
    @State var offsetX: CGFloat = 0
    @State var isSelected: Bool = false
    private var place: TripAdvisorRestaurantModel
    
    init(_ place: TripAdvisorRestaurantModel,
         isSelected: Bool
    ) {
        self.place = place
        self.isSelected = isSelected
    }
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading) {
                Text(place.name)
                    .foregroundStyle(.white)
                    .fontWeight(.black)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .multilineTextAlignment(.leading)
                Text(place.address ?? "")
                    .foregroundStyle(.white)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .multilineTextAlignment(.leading)
                RatingBar(.stars(place.rating ?? 0))
            }
            .padding()
            .background {
                backgroundImage()
                    .overlay(.ultraThinMaterial)
            }
            .clipShape(RoundedRectangle(cornerRadius: 20))
            .padding(.horizontal, 10)
            .offset(x: offsetX)
            .gesture(
                DragGesture()
                    .onChanged { gesture in
                        if gesture.translation.width < 0 {
                            offsetX = gesture.translation.width
                        } else {
                            offsetX = 0
                        }
                    }
                    .onEnded{ gesture in
                        switch gesture.translation.width {
                        case ...0 :
                            offsetX = -50
                        case 0... :
                            offsetX = 0
                        default:
                            break
                        }
                    }
            )
            
            Image(.favSelectIcon)
                .resizable()
                .frame(width: 30, height: 30)
                .frame(maxWidth: .infinity, alignment: .trailing)
                .padding(.horizontal)
                .opacity(offsetX == 0 ? 0 : 1)

        }
        .animation(.snappy, value: offsetX)
    }
    
    @ViewBuilder
    func backgroundImage() -> some View {
        if let stringURL = place.photoUrls?[0],
           let url = URL(string: stringURL) {
            KFImage(url)
                .placeholder {
                    ProgressView()
                }
                .resizable()
                .aspectRatio(contentMode: .fill)
                .clipped()
        }
    }
}
