//
//  MainViewModel.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import SwiftUI
import Combine
import CoreLocation

enum ScreenConditionType {
    case loading
    case loaded
    case empty
    case previoslyShowed
    case error
}

class MainViewModel: ObservableObject {
    
    var utilities = Utilities()
    private var core: Core
    private var dispose = Set<AnyCancellable>()
    private var filter: [String : Any] = [:]
    private let action: PassthroughSubject<Action, Never> = .init()
    private var lastShowedRestaurants: [Int] = []
    private var navBarState: Bool = false
    private var counter: Int = 0
    @Published var screenCondition: ScreenConditionType = .loading
    @Published var restaurant: TripAdvisorRestaurantModel = .empty
    @Published var places: [TripAdvisorRestaurantModel] = []
    @Published var repeatButton: HedonistButtonView.ViewModel?
    @Published var removeButton: HedonistButtonView.ViewModel?
    @Published var clearFiltersVM: HedonistButtonView.ViewModel

    @Published var navigationBarVM: NavigationBarView.ViewModel
    @Published var filterVM: FilterViewModel?
    
    @Published var link: Link? { didSet { isLinkActive = link != nil } }
    @Published var appearance = 1
    @Published var isLinkActive: Bool = false
    @Published var isFilterShowed: Bool = false
    @Published var isFavorite: Bool = false
    @Published var isRecommend: Bool = true
    @Published var removeButtonVisible: Bool = false
    @Published var restaurantsListVisible: Bool = false
    @Published var isDetailCardVisible: Bool = false
    @Published var scrollDisabled: Bool = true

    init(core: Core, repeatButton: HedonistButtonView.ViewModel?,
         removeButton: HedonistButtonView.ViewModel?,
         clearFiltersVM: HedonistButtonView.ViewModel,
         navigationBarVM: NavigationBarView.ViewModel,
         filterVM: FilterViewModel?
    ) {
        self.core = core
        self.repeatButton = repeatButton
        self.removeButton = removeButton
        self.clearFiltersVM = clearFiltersVM
        self.navigationBarVM = navigationBarVM
        self.filterVM = filterVM
    }
    
    convenience init(core: Core, filter: [String : Any] = [:]) {
        self.init(core: core,
                  repeatButton: nil,
                  removeButton: nil,
                  clearFiltersVM: .init(title: "Сбросить", titleColor: Color(.orange), color: Color(.clear), action: {}),
                  navigationBarVM: .init(title: .text("")),
                  filterVM: nil)
        self.filter = filter
        self.repeatButton = .init(title: "Выбрать новый", titleColor: .white, color: .black, action: {
            self.updateRestaurant(isInitial: false)
        })
        self.removeButton = .init(title: "Такого не существует", titleColor: .black, color: .gray.opacity(0.2), action: deleteRestaurant)
        self.navigationBarVM.rightButtons = [NavigationBarView.ViewModel.ButtonViewModel(icon: Image(systemName: "person.crop.circle"), action: self.pushToProfile)]
        self.navigationBarVM.leftButtons = [
            NavigationBarView.ViewModel.ButtonViewModel(icon: Image(systemName: "heart.circle"), action: self.showFavourites),
            NavigationBarView.ViewModel.ButtonViewModel(icon: Image(systemName: "gearshape.circle"), action: self.showFilters)
        ]
        self.clearFiltersVM.action = {
            core.action.send(CoreModelAction.PersistantAction.DeleteValue())
            core.action.send(CoreModelAction.RestaurantManagerAction.GetNewItem.init(value: [:]))
        }
        self.filterVM = .init(core: core, action: updateFilters)
        bind()
        
    }
    
    private func bind() {
        core.selectedFilters
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] properties in
                var localCuisinesContainer: [String] = []
                var localNeighborhoodContainer: [String] = []
                var localDietaryContainer: [String] = []
                var localFeaturesContainer: [String] = []
                
                properties.forEach { cell in
                    
                    switch cell.type {
                    case .cuisines:
                        localCuisinesContainer.append(cell.title)
                        
                    case .neighborhood:
                        localNeighborhoodContainer.append(cell.title)
                        
                    case .dietaryrestrictions:
                        localDietaryContainer.append(cell.title)
                        
                    case .features:
                        localFeaturesContainer.append(cell.title)
                        
                    }
                }
                
                filter["cuisines"] = localCuisinesContainer
                filter["neighborhood"] = localNeighborhoodContainer
                filter["dietaryrestrictions"] = localDietaryContainer
                filter["features"] = localFeaturesContainer
            }
            .store(in: &dispose)
        
        core.restaurants
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] restaurants in
                while counter <= 3, restaurants.isEmpty {
                    updateRestaurant()
                    return
                }
                
                guard !restaurants.isEmpty else {
                    screenCondition = .empty
                    return
                }
                screenCondition = .loaded
                places = restaurants
                getRandomRestaurant()
            }
            .store(in: &dispose)
        
        core.favoriteRestaurants
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] places in
                self.isFavorite = places.compactMap {$0.id}.contains(restaurant.id)
            }
            .store(in: &dispose)
        
        action
            .receive(on: DispatchQueue.main)
            .sink {[unowned self] action in
                switch action {
                case _ as CoreModelAction.MainViewActions.FavoritesTapped:
                    link = .favoritesView(.init(core: core))
                case _ as CoreModelAction.MainViewActions.ProfileTapped:
                    link = .profileView(.init(core: core))
                default: break
                }
            }
            .store(in: &dispose)
    }
    
    func getRandomRestaurant(){
        if restaurant == .empty,
           let randomPlace = places.randomElement() {
            restaurant = randomPlace
            counter = 0
        } else {
            updateRestaurant()
        }
    }
    
    func updateNavigationBarVisibility() {
        withAnimation(.interactiveSpring(response: 0.6, dampingFraction: 0.7, blendDuration: 0.7)) {
            self.navBarState.toggle()
            appearance = navBarState ? 1 : 0
            isDetailCardVisible.toggle()
        }
    }
    
    enum Link {
        case detailedView(TripAdvisorRestaurantModel)
        case favoritesView(FavoritesViewModel)
        case profileView(ProfileViewModel)
    }
}

extension MainViewModel {
    func showDetailView() {
        link = .detailedView(restaurant)
    }
    
    func updateDetailView(_ state: Bool) {
        withAnimation(.spring) {
            self.isDetailCardVisible = state
        }
    }
    
    func showFilters() {
        removeButtonVisible = false
        isFilterShowed = true
    }
    
    func showFavourites() {
        removeButtonVisible = false
        action.send(CoreModelAction.MainViewActions.FavoritesTapped())
    }
    
    func updateFilters() {
        removeButtonVisible = false
        restaurantsListVisible = false
        isDetailCardVisible = false
        scrollDisabled = true
        isFilterShowed = false
    }
    
    
    func pushToProfile() {
        action.send(CoreModelAction.MainViewActions.ProfileTapped())
    }
    
    func updateRestaurant(isInitial: Bool = true) {
        if !isInitial {
            lastShowedRestaurants.append(restaurant.id)
        }
        self.removeButtonVisible = false
        screenCondition = .loading
        restaurant = .empty
        if lastShowedRestaurants.count > places.count {
            screenCondition = .previoslyShowed
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.25) {
            self.core.action.send(CoreModelAction.RestaurantManagerAction.GetNewItem(value: self.filter))
            self.counter += 1
        }
        
    }
    
    func updateFavItem(item: TripAdvisorRestaurantModel) async {
        core.action.send(CoreModelAction.RestaurantManagerAction.UpdateFavItem(value: item))
    }
    
    func deleteRestaurant() {
        self.removeButtonVisible = false
        screenCondition = .loading
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.25) {
            self.core.action.send(CoreModelAction.RestaurantManagerAction.DeleteCurrent(valueFilters: self.filter, valueRestaurant: self.restaurant))
        }
        
    }
}
