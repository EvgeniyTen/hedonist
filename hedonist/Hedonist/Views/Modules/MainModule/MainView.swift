//
//  MainView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 21.05.2023.
//

import SwiftUI

struct MainView: View {
    
    @ObservedObject var viewModel: MainViewModel
    @AppStorage("selectedAppearance") var selectedAppearance = 0
    @State var scrollIndex: Int = 0
    @Namespace var animation
    var body: some View {
        ZStack(alignment: .bottom) {
            VStack(spacing: 10) {
                screenContent
                buttonsView
                VStack {
                    RoundedRectangle(cornerRadius: 20)
                        .frame(width: 50, height: 5)
                        .frame(maxWidth: .infinity, maxHeight: 10)
                    
                    Text("Все рестораны")
                        .multilineTextAlignment(.leading)
                        .fontWeight(.bold)
                        .frame(maxWidth: .infinity, maxHeight: 10, alignment: .leading)
                        .padding(.horizontal)

                    RestaurantsListView(
                        scrollDisabled: $viewModel.scrollDisabled,
                        scrollIndex: $scrollIndex,
                        restaurants: viewModel.places) { restaurant in
                            Task {
                                await viewModel.updateFavItem(item: restaurant)
                            }
                        }
                }
                .opacity(
                    viewModel.removeButtonVisible
                    ? viewModel.restaurantsListVisible
                    ? 1
                    : 0.1
                    : 0)
                .frame(maxHeight: viewModel.restaurantsListVisible
                       ? screenHeight(by: 1)
                       : viewModel.removeButtonVisible
                       ? .infinity
                       : 0)
                .handleVerticalSwipe { direction in
                    switch direction {
                    case .up:
                        viewModel.scrollDisabled = false
                        viewModel.restaurantsListVisible = true
                    case .down:
                        viewModel.scrollDisabled = true
                        viewModel.restaurantsListVisible = false
                        viewModel.removeButtonVisible = false

                    default:
                        break
                    }
                }
            }
            .edgesIgnoringSafeArea(.bottom)
            
            .padding(.horizontal)
            .frame(maxHeight: .infinity)
            
            NavigationLink("", isActive: $viewModel.isLinkActive) {
                if let link = viewModel.link  {
                    switch link {
                    case .detailedView(let restaurant):
                        DetailedCardView(
                            place: restaurant,
                            action: viewModel.updateNavigationBarVisibility,
                            favAction: viewModel.updateFavItem
                        ) {
                            regularCard
                        }
                    case .favoritesView(let viewModel):
                        FavoritesView(viewModel: viewModel)
                    case .profileView(let viewModel):
                        ProfileView(viewModel: viewModel)
                    }
                }
            }
        }
        
        .animation(.snappy, value: viewModel.removeButtonVisible)
        .navigationBar(with: viewModel.navigationBarVM)
        .onChange(of: viewModel.appearance) { _, newValue in
            selectedAppearance = newValue
        }
        
        .onChange(of: selectedAppearance) { _, newValue in
            viewModel.utilities.overrideDisplayMode(newValue)
        }
        
        .fullScreenCover(isPresented: $viewModel.isFilterShowed) {
            if let filterVM = viewModel.filterVM {
                FilterView(viewModel: filterVM)
            }
        }
//        .overlay {
//            if viewModel.isDetailCardVisible {
//                ZStack {
//                    Color.white.ignoresSafeArea()
//                    DetailedCardView(place: viewModel.restaurant, action: viewModel.updateNavigationBarVisibility, favAction: viewModel.updateFavItem) {
//                        screenContent
//                    }
//                    .scaleEffect(viewModel.isDetailCardVisible ? 1 : 0)
//                }
//            }
//        }
    }
}

extension MainView {
    
    var isLoading: Bool {
        viewModel.screenCondition == .loading
    }
    
    var isEmpty: Bool {
        viewModel.screenCondition == .empty
    }
    
    func emptyDataScreen(_ type: ScreenConditionType) -> some View {
        ZStack(alignment: .top) {
            Image(.emptyResult)
                .opacity(0.5)
                .blur(radius: 1.0)
            VStack {
                switch type {
                case .previoslyShowed:
                    Text("Похоже, что у нас кончились рестораны")
                        .foregroundStyle(.black)
                    Text("необходимо изменить фильтры")
                        .foregroundStyle(.black)
                default:
                    Text("Очень лютые фильтры")
                        .foregroundStyle(.black)
                    Text("давай полегче")
                        .foregroundStyle(.black)
                }
                
                HedonistButtonView(viewModel: viewModel.clearFiltersVM)
                    .padding(.horizontal, 20)
            }
            .frame(minHeight: 200, alignment: .bottom)
            .foregroundStyle(.white)
            .fontWeight(.bold)
        }
    }
    
    var regularCard: some View {
        RegularCardView(place: viewModel.restaurant,
                        isDetailed: viewModel.isDetailCardVisible)
    }
    
    
    
    @ViewBuilder
    var screenContent: some View {
        ZStack {
            Button {
    //            viewModel.updateDetailView(true)
                viewModel.showDetailView()
            } label: {
                regularCard
    //            .matchedGeometryEffect(id: "Hero", in: animation)
            }
                .opacity(viewModel.screenCondition == .loaded ? 1 : 0)
                .padding(.horizontal)

            LoaderView()
                .opacity(viewModel.screenCondition == .loading ? 1 : 0)
            emptyDataScreen(.error)
                .opacity(viewModel.screenCondition == .empty ? 1 : 0)
            emptyDataScreen(.previoslyShowed)
                .opacity(viewModel.screenCondition == .previoslyShowed ? 1 : 0)
        }
        .opacity(viewModel.restaurantsListVisible ? 0.3 : 1)
        .isVisible(!viewModel.restaurantsListVisible)
        .clipShape(.rect(cornerRadius: 25))
        .allowsHitTesting(!viewModel.restaurantsListVisible)
        .handleVerticalSwipe { direction in
            switch direction {
            case .up:
                viewModel.removeButtonVisible = true
            case .down:
                viewModel.scrollDisabled = true
                viewModel.removeButtonVisible = false
                viewModel.restaurantsListVisible = false
            default:
                break
            }
        }
    }
    
    var buttonsView: some View {
        VStack {
            if let repeatButton = viewModel.repeatButton {
                HedonistButtonView(viewModel: repeatButton)
                    .allowsHitTesting(!viewModel.restaurantsListVisible)
            }
            if let removeButton = viewModel.removeButton, viewModel.removeButtonVisible {
                 HedonistButtonView(viewModel: removeButton)
                    .opacity(viewModel.removeButtonVisible ? 1 : 0)
                    .allowsHitTesting(!viewModel.restaurantsListVisible)
             }
        }
        .frame(maxHeight: viewModel.restaurantsListVisible
               ? 0
               : viewModel.removeButtonVisible
               ? screenHeight(by: 0.2)
               : screenHeight(by: 0.1))
        .padding(.horizontal)
        .clipped()
        .opacity(viewModel.restaurantsListVisible ? 0.3 : 1)
        .handleVerticalSwipe { direction in
            switch direction {
            case .down:
                viewModel.removeButtonVisible = false
                viewModel.restaurantsListVisible = false
            default:
                break
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(viewModel: .init(core: .emptyMock))
    }
}
