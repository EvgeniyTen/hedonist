//
//  FilterView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 05.06.2023.
//

import SwiftUI

enum ExpandButtonType {
    case features
    case cuisines
}

struct FilterView: View {
    @ObservedObject var viewModel: FilterViewModel
    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                filtersView
                buttonsStackView
            }
            .searchable(text: $viewModel.searchText,
                        placement: .navigationBarDrawer(displayMode: .always),
                        prompt: .init("Укажите Ваше предпочтение"))
        }
        .onAppear {
            viewModel.fetchFiltersIfNeeded()
        }
        .onDisappear {
            viewModel.cells.removeAll()
        }
        .fullScreenCover(isPresented: $viewModel.isNeighborsVisible, onDismiss: {
            viewModel.isNeighborsVisible = false
        }, content: {
            NeighborhoodsListView(viewModel: viewModel.neighborhoodsViewModel)
        })
        .navigationBarBackButtonHidden()
    }
    
    func isCountMoreThan5(_ data: [FilterCellModel]) -> Bool {
        data.count >= 5
    }
}

struct FilterView_Previews: PreviewProvider {
    static var previews: some View {
        FilterView(viewModel: .init(core: .emptyMock))
    }
}


extension FilterView {
    var buttonsStackView: some View {
        VStack {
            if viewModel.isEditing {
                if let nextButton = viewModel.nextButton {
                    HedonistButtonView(viewModel: nextButton)
                }
                if let clearButton = viewModel.clearButton {
                    HedonistButtonView(viewModel: clearButton)
                }
            } else {
                if let clearButton = viewModel.clearButton,
                   !viewModel.filters.isEmpty {
                    HedonistButtonView(viewModel: clearButton)
                }
                if let closeButton = viewModel.closeButton {
                    HedonistButtonView(viewModel: closeButton)
                }
            }
        }
        .padding()
        .background(
            RoundedRectangle(cornerRadius: 25.0)
                .foregroundColor(.gray.opacity(0.2))
                .ignoresSafeArea()
        )
        .ignoresSafeArea(.keyboard)
    }
    
    private func titleBy(_ type: FilterCellType) -> String {
        switch type {
        case .cuisines:
            return "Тип Кухни"
        case .dietaryrestrictions:
            return "Особенности"
        case .features:
            return "Удобства"
        case .neighborhood:
            return "Район"
        }
    }
    
    @ViewBuilder
    private func filtersTitle(_ type: FilterCellType) -> some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(alignment: .center) {
                Text(titleBy(type))
                    .padding(.vertical, 10)
                    .foregroundColor(.black)
                    .font(.headline)
                    .padding(.horizontal)
                chosenFilters(type)
            }
        }
    }
    
    var neighborhoodPicker: some View {
        Button {
            viewModel.isNeighborsVisible = true
        } label: {
            HStack {
                    if viewModel.selectedCategory != "" {
                        Text(viewModel.selectedCategory)
                            .foregroundStyle(.black)
                            .frame(maxWidth: .infinity, alignment: .leading)
                    } else {
                        Text("Выбрать свой район")
                            .foregroundStyle(.black)
                            .frame(maxWidth: .infinity, alignment: .leading)
                    }
                    Image(systemName: "chevron.right")
                        .renderingMode(.template)
                        .foregroundStyle(.black)
            }
            .padding()
            .background {
                RoundedRectangle(cornerRadius: 20)
                    .stroke(Color("themeOrange"), lineWidth: 1)
                    .padding(1)
            }
            .tint(.black)
        }
    }
    
    func expandButton(_ type: ExpandButtonType) -> some View {
        Button {
            switch type {
            case .features:
                viewModel.expandFeatures()
            case .cuisines:
                viewModel.expandCuisines()
            }
        } label: {
            switch type {
            case .features:
                Text(viewModel.isFeaturesExpanded 
                     ? "Показать меньше" 
                     : "Показать больше")
            case .cuisines:
                Text(viewModel.isCuisinesExpanded 
                     ? "Показать меньше" 
                     : "Показать больше")
            }
        }
        .padding(.leading)
    }
    
    var cuisinesData: Int {
        viewModel.isCuisinesExpanded
        ? viewModel.cuisines.count - 1
        : isCountMoreThan5(viewModel.cuisines)
        ? 6
        : viewModel.cuisines.count - 1
    }
    
    var featuresData: Int {
        viewModel.isFeaturesExpanded
        ? viewModel.features.count - 1
        : isCountMoreThan5(viewModel.features)
        ? 6
        : viewModel.features.count - 1
    }
    
    var filtersView: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading) {
                if !viewModel.cuisines.isEmpty {
                    VStack(alignment: .leading) {
                        filtersTitle(.cuisines)
                        TagLayout {
                            ForEach(0..<cuisinesData, id: \.self) { index in
                                FilterCell(model: viewModel.cuisines[index], action: viewModel.updateFilter)
                                    .padding(.leading, 10)
                            }
                        }
                        expandButton(.cuisines)
                    }
                }
                
                if !viewModel.dietaryRestrictions.isEmpty {
                    VStack(alignment: .leading) {
                        filtersTitle(.dietaryrestrictions)
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack {
                                ForEach(viewModel.dietaryRestrictions, id: \.self) {
                                    FilterCell(model: $0, action: viewModel.updateFilter)
                                        .padding(.leading, 10)
                                }
                            }
                        }
                    }
                }
                
                if !viewModel.features.isEmpty {
                    VStack(alignment: .leading) {
                        filtersTitle(.features)
                        fastFiltersCarousel
                        TagLayout {
                            ForEach(0..<featuresData, id: \.self) { index in
                                FilterCell(
                                    model: viewModel.features[index],
                                    action: viewModel.updateFilter)
                                    .padding(.leading, 10)
                            }
                        }
                        expandButton(.features)
                    }
                }
                
                if !viewModel.neighborhood.isEmpty {
                    VStack(alignment: .leading) {
                        Text("Район")
                            .padding(.top, 20)
                            .foregroundColor(.black)
                            .font(.headline)
                            .padding(.horizontal)
                        neighborhoodPicker
                    }
                    .padding(.horizontal)
                }
            }
        }
        .frame(maxHeight: .infinity, alignment: .top)
    }
    
    var fastFiltersCarousel: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(FastFilter.allCases.indices, id: \.self) { index in
                    Button{
                        selectMultiply(FastFilter.allCases[index])
                    } label: {
                        let imageResource = ImageResource(name: FastFilter.allCases[index].rawValue, bundle: .main)
                        Image(imageResource)
                            .resizable()
                            .frame(width: 30, height: 30)
                            .padding()
                            .if(selectionState(FastFilter.allCases[index])) { view in
                                view
                                    .background(
                                        Circle()
                                            .foregroundStyle(Color("themeOrange"))
                                    )
                            } else: { view in
                                view
                                    .background(
                                        Circle()
                                            .stroke(Color("themeOrange"))
                                            .padding(2)
                                    )
                            }
                    }
                    .listPaddings(index: index, collectionCount: FastFilter.allCases.count)
                }
            }
        }
    }
    func selectMultiply(_ type: FastFilter) {
        switch type {
        case .pets:
            if let cell = viewModel.filterCells.first(where: {
                $0.title == "Для гостей с собаками"
            }) {
                viewModel.updateFilter(value: cell.title)
            }
        case .wifi:
            if let cell = viewModel.filterCells.first(where: {
                $0.title == "Бесплатный Wi-Fi"
            }) {
                viewModel.updateFilter(value: cell.title)
            }
        case .parking:
            if let cell1 = viewModel.filterCells.first(where: {$0.title == "Имеется парковка"}),
               let cell2 = viewModel.filterCells.first(where: {$0.title == "Парковка на улице"})
            {
                viewModel.updateFilter(value: cell1.title)
                viewModel.updateFilter(value: cell2.title)
            }
        case .chair:
            if let cell = viewModel.filterCells.first(where: {
                $0.title == "Детские стульчики для кормления"
            }) {
                viewModel.updateFilter(value: cell.title)
            }
        case .alcohol:
            if let cell1 = viewModel.filterCells.first(where: {$0.title == "Подают алкоголь"}),
               let cell2 = viewModel.filterCells.first(where: {$0.title == "Бар"}),
               let cell3 = viewModel.filterCells.first(where: {$0.title == "Вино и пиво"})
            {
                let cells = [cell1,
                             cell2,
                             cell3]
                cells.forEach { cell in
                    viewModel.updateFilter(value: cell.title)
                }
            }
        }
    }
    func selectionState(_ type: FastFilter) -> Bool {
        switch type {
        case .pets:
            viewModel.filterCells.first(where: {
                $0.title == "Для гостей с собаками"
            })?.isSelected ?? false
        case .wifi:
            viewModel.filterCells.first(where: {
                $0.title == "Бесплатный Wi-Fi"
            })?.isSelected ?? false
        case .parking:
            viewModel.filterCells.first(where: {
                $0.title == "Имеется парковка"
                || $0.title == "Парковка на улице"
            })?.isSelected ?? false
            
        case .chair:
            viewModel.filterCells.first(where: {
                $0.title == "Детские стульчики для кормления"
            })?.isSelected ?? false
            
        case .alcohol:
            viewModel.filterCells.first(where: {
                $0.title == "Подают алкоголь"
                || $0.title == "Бар"
                || $0.title == "Вино и пиво"
            })?.isSelected ?? false
            
        }
    }
    
    @ViewBuilder func chosenFilters(_ type: FilterCellType) -> some View {
        let chosenValues = viewModel.filters
            .filter { $0.key == type.rawValue }
            .flatMap { $0.value }
            .sorted()
        if let title = chosenValues.count > 1 ? "\(chosenValues.first ?? "") +\(chosenValues.count - 1)" : chosenValues.first {
            Text(title)
                .foregroundStyle(.gray)
                .padding(.vertical, 5)
                .padding(.horizontal)
                .background {
                    Capsule()
                        .foregroundStyle(.gray.opacity(0.2))
                }
        }
    }
}
