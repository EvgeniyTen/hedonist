//
//  FilterCell.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 18.06.2023.
//

import SwiftUI

enum FilterCellType: String {
    case cuisines
    case neighborhood
    case dietaryrestrictions
    case features
}

struct FilterCellModel: Hashable, Identifiable {
    var id: UUID = UUID()
    var isSelected: Bool
    var title: String
    var type: FilterCellType
    
    mutating func toggleSelection() {
        isSelected.toggle()
    }
}

struct FilterCell: View {
    var model: FilterCellModel
    var action: (String) -> Void
    
    var body: some View {
        Text(model.title)
            .fontWeight(.thin)
            .foregroundColor(model.isSelected ? .white : .black)
            .padding(20)
            .background {
                RoundedRectangle(cornerRadius: 20)
                    .stroke(Color("themeOrange"), lineWidth: 1)
                    .padding(5)
            }
            .background {
                RoundedRectangle(cornerRadius: 20)
                    .foregroundColor(model.isSelected ? Color("themeOrange") : .clear)
                    .padding(5)
            }
            .onTapGesture {
                withAnimation(.spring()) {
                    updateSelection()
                }
            }
    }
    
    func updateSelection() {
        action(model.title)
    }
    
}

struct FilterCell_Previews: PreviewProvider {
    static var previews: some View {
        FilterCell(model: .init(isSelected: false, title: "Chineese", type: .cuisines), action: {_ in})
    }
}
