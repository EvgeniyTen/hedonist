//
//  FilterViewModel.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 05.06.2023.
//

import Foundation
import SwiftUI
import Combine

enum FastFilter: String, CaseIterable {
    case pets = "pet-friendly"
    case wifi = "wifi-sign"
    case parking = "parking-sign"
    case chair = "chair-sign"
    case alcohol = "alcohol-sign"
}

class FilterViewModel: ObservableObject {
    @AppStorage("wasOnboarded") var wasOnboarded: Bool = false
    
    private let core: Core
    var cuisines: [FilterCellModel] {
        filterCells
            .filter({ $0.type == .cuisines })
            .sorted(by: {$0.isSelected && !$1.isSelected})
            .sorted(by: {$0.title < $1.title})
    }
    
    var dietaryRestrictions: [FilterCellModel] {
        filterCells
            .filter({ $0.type == .dietaryrestrictions })
            .sorted(by: {$0.isSelected && !$1.isSelected})
            .sorted(by: {$0.title < $1.title})
    }
    
    var features: [FilterCellModel] {
        filterCells
            .filter({ $0.type == .features })
            .sorted(by: {$0.isSelected && !$1.isSelected})
            .sorted(by: {$0.title < $1.title})
    }


    var neighborhood: [FilterCellModel] {
        filterCells
            .filter({ $0.type == .neighborhood })
            .sorted(by: {$0.isSelected && !$1.isSelected})
            .sorted(by: {$0.title < $1.title})
    }
    @Published var selectedCategory: String = "Не выбрано"
    @Published var cells: [FilterCellModel] = [] {
        willSet {
            filterCells = newValue
        }
    }
    private var dispose = Set<AnyCancellable>()
    
    var utilities = Utilities()
    @Published var isCuisinesExpanded: Bool = false
    @Published var isFeaturesExpanded: Bool = false
    @Published var isNeighborsVisible: Bool = false
    @Published var isEditing: Bool = false
    @Published var descriptionBlock: TitleSubtitleBlock.ViewModel?
    @Published var nextButton: HedonistButtonView.ViewModel?
    @Published var clearButton: HedonistButtonView.ViewModel?
    @Published var closeButton: HedonistButtonView.ViewModel?
    @Published var neighborhoodsViewModel: NeighborhoodsListView.ViewModel
    
    @Published var filters = [String : [String]]()
    @Published var filterCells: [FilterCellModel] = []
    @Published var searchText: String = "" {
        willSet {
            if newValue.isEmpty {
                filterCells = cells
            } else {
                filterCells = cells.filter {
                    $0
                        .title
                        .lowercased()
                        .contains(newValue.lowercased())
                }
            }
        }
    }
    
    init(core: Core,
         descriptionBlock: TitleSubtitleBlock.ViewModel?,
         neighborhoodsViewModel: NeighborhoodsListView.ViewModel,
         nextButton: HedonistButtonView.ViewModel?,
         clearButton: HedonistButtonView.ViewModel?,
         closeButton: HedonistButtonView.ViewModel?) {
        self.core = core
        self.descriptionBlock = descriptionBlock
        self.neighborhoodsViewModel = neighborhoodsViewModel
        self.nextButton = nextButton
        self.clearButton = clearButton
        self.closeButton = closeButton
    }
    
    convenience init(core: Core, action: @escaping () -> Void) {
        self.init(core: core,
                  descriptionBlock: .init(
                    titleText: "",
                    titleImageName: nil,
                    subtitleText: "",
                    subtitleImageName: nil),
                  
                  neighborhoodsViewModel: .init(
                    selectedHeighbor: "",
                    neighborhoods: [],
                    onTap: { _ in }),
                  
                  nextButton: .init(
                    title: "Применить",
                    titleColor: .white,
                    color: Color("themeOrange"),
                    action: {}),
                  
                  clearButton: .init(
                    title: "Очистить",
                    titleColor: Color("themeOrange"),
                    action: {}),
                  
                  closeButton: .init(
                    title: "Закрыть",
                    titleColor: Color("themeOrange"),
                    action: {}))
        self.neighborhoodsViewModel.onTap = selectNeighborhood
        self.nextButton?.action = { self.updateTapepd(); action()}
        self.clearButton?.action = { self.clearAction(); action()}
        self.closeButton?.action = action
        bind()
    }
    
    convenience init(core: Core) {
        self.init(core: core,
                  descriptionBlock: .init(
                    titleText: "",
                    titleImageName: nil,
                    subtitleText: "",
                    subtitleImageName: nil),
                  
                  neighborhoodsViewModel: .init(
                    selectedHeighbor: "",
                    neighborhoods: [],
                    onTap: { _ in }),

                  nextButton: .init(
                    title: "Применить",
                    titleColor: .white,
                    color: Color("themeOrange"),
                    action: {}),
                  
                  clearButton: .init(
                    title: "Очистить",
                    titleColor: Color("themeOrange"),
                    action: {}),
                  
                  closeButton: .init(
                    title: "Закрыть",
                    titleColor: .white,
                    color: Color("themeOrange"),
                    action: {}))
        
        self.neighborhoodsViewModel.onTap = selectNeighborhood
        self.nextButton?.action = nextTapped
        self.closeButton?.action = nextTapped
        self.clearButton?.action = clearAction

        bind()
        
    }
    
    func bind() {
        fetchFiltersIfNeeded()

        core.action
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] action in
                switch action {
                case _ as CoreModelAction.RestaurantManagerAction.GetNewItem:
                    selectedCategory = "Не выбрано"
                    neighborhoodsViewModel.selectedHeighbor = nil

                default:
                    break
                }
            }
            .store(in: &dispose)
        

        core.restaurantProperties
            .combineLatest(core.selectedFilters)
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] properties, selecterFilters in
                if let cuisinesProperties = properties["cuisines"] as? [String] {
                    cuisinesProperties.forEach { title in
                        if !cells.contains(where: { $0.title == title}) {
                            cells.append(
                                FilterCellModel(
                                    isSelected: selecterFilters.contains(where: { $0.title == title }),
                                    title: title,
                                    type: .cuisines))
                        }
                    }
                }
                
                if let neighborhoodProperties = properties["neighborhood"] as? [String] {
                    neighborhoodsViewModel.neighborhoods = neighborhoodProperties
                    neighborhoodProperties.forEach { title in
                        if !cells.contains(where: { $0.title == title}) {
                            cells.append(
                                FilterCellModel(
                                    isSelected: selecterFilters.contains(where: { $0.title == title }),
                                    title: title,
                                    type: .neighborhood))
                        }
                    }
                }
                
                if let dietaryRestrictionsProperties = properties["dietaryrestrictions"] as? [String] {
                    dietaryRestrictionsProperties.forEach { title in
                        if !cells.contains(where: { $0.title == title}) {
                            cells.append(
                                FilterCellModel(
                                    isSelected: selecterFilters.contains(where: { $0.title == title }),
                                    title: title,
                                    type: .dietaryrestrictions))
                        }
                    }
                }
                
                if let features = properties["features"] as? [String] {
                    features.forEach { title in
                        if !cells.contains(where: { $0.title == title}) {
                            cells.append(
                                FilterCellModel(
                                    isSelected: selecterFilters.contains(where: { $0.title == title }),
                                    title: title,
                                    type: .features))
                        }
                    }
                }
                
                var localCuisinesContainer: [String] = []
                var localNeighborhoodValue: [String] = []
                var localDietaryContainer: [String] = []
                var localFeaturesContainer: [String] = []
                
                selecterFilters.forEach { cell in
                    
                    switch cell.type {
                    case .cuisines:
                        localCuisinesContainer.append(cell.title)
                    case .neighborhood:
                        localNeighborhoodValue.append(cell.title)
                    case .dietaryrestrictions:
                        localDietaryContainer.append(cell.title)
                    case .features:
                        localFeaturesContainer.append(cell.title)
                    }
                }
                

                filters["cuisines"] = localCuisinesContainer
                if localCuisinesContainer.isEmpty {
                    filters.removeValue(forKey: "cuisines")
                }

                filters["neighborhood"] = localNeighborhoodValue
                if let lastSelected = localNeighborhoodValue.last {
                    selectedCategory = lastSelected
                    neighborhoodsViewModel.selectedHeighbor = lastSelected
                } else {
                    neighborhoodsViewModel.selectedHeighbor = nil
                }
                if localNeighborhoodValue.isEmpty {
                    filters.removeValue(forKey: "neighborhood")
                }
                
                filters["dietaryrestrictions"] = localDietaryContainer
                if localDietaryContainer.isEmpty {
                    filters.removeValue(forKey: "dietaryrestrictions")
                }
                
                filters["features"] = localFeaturesContainer
                if localFeaturesContainer.isEmpty {
                    filters.removeValue(forKey: "features")
                }
            }
            .store(in: &dispose)

    }

    func fetchFiltersIfNeeded() {
        if cells.isEmpty {
            core.action.send(CoreModelAction.RestaurantManagerAction.GetProperties())
        }
    }
    
    func expandCuisines() {
        withAnimation(.easeInOut) {
            isCuisinesExpanded.toggle()
        }
    }
    
    func expandFeatures() {
        withAnimation(.easeInOut) {
            isFeaturesExpanded.toggle()
        }
    }
    
    @MainActor
    private func update(_ name: String, filter: String) {
        core.action.send(CoreModelAction.PersistantAction.UpdateValue(name: name, filter: filter))
    }
    
    @MainActor
    private func clearAll() {
        core.action.send(CoreModelAction.PersistantAction.DeleteValue())
        isEditing = false
    }
    
    func updateFilter(value: String) {
        Task {
            await MainActor.run {
                if let index = cells.firstIndex(where: { $0.title == value}) {
                    cells[index].isSelected.toggle()
                    searchText = ""
                    update(cells[index].type.rawValue.lowercased(), filter: value)
                    isEditing = true
                }
            }
        }
    }
    
    func selectNeighborhood(title: String) {
        updateFilter(value: title)
    }

    private func clearAction() {
        core.action.send(CoreModelAction.PersistantAction.DeleteValue())
        core.action.send(CoreModelAction.RestaurantManagerAction.GetNewItem.init(value: [:]))
        isEditing = false
        selectedCategory = "Не выбрано"
        neighborhoodsViewModel.selectedHeighbor = nil
    }
    
    private func updateTapepd() {
        core.action.send(CoreModelAction.RestaurantManagerAction.GetNewItem.init(value: filters))
        searchText = ""
        isEditing = false
    }
    
    private func nextTapped() {
        core.action.send(CoreModelAction.RestaurantManagerAction.GetNewItem.init(value: filters))
        wasOnboarded = true
        isEditing = false
    }
}

