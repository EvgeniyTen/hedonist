//
//  ProfileView.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 25.07.2023.
//

import SwiftUI

struct ProfileView: View {
    @ObservedObject var viewModel: ProfileViewModel

    var body: some View {
        ZStack {
            VStack {
//                VStack {
//                    Image(.avatarPlaceholder)
//                        .resizable()
//                        .scaledToFill()
//                        .frame(width: 80, height: 80)
//                        .clipShape(Circle())
//                        .overlay(Circle().stroke(Color.gray, lineWidth: 1))
//
//                    Text(viewModel.userData.name.capitalized)
//                        .font(.title)
//                        .padding(.top, 8)
//                        .foregroundStyle(.black)
//                }
//                .padding()
                
//                Text("Настройки")
//                    .font(.headline)
//                    .frame(maxWidth: .infinity, alignment: .leading)
//                
                ScrollView(.vertical, showsIndicators: false) {
                    if viewModel.isLogged {
                        if !viewModel.isNotificationsEnabled {
                            Button {
                                viewModel.notificationsRequest()
                            } label: {
                                HStack {
                                    Text("Включить уведомления")
                                        .foregroundStyle(.black)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                    Toggle("", isOn: $viewModel.isNotificationsEnabled)
                                        .frame(maxWidth: 50)
                                        .allowsHitTesting(false)
                                }
                                .padding()
                                .frame(maxHeight: 50)
                                .background {
                                    RoundedRectangle(cornerRadius: 20)
                                        .stroke(LinearGradient(colors: [.red, .orange, .black.opacity(0.3), .black], startPoint: .topLeading, endPoint: .bottomTrailing), lineWidth: 1)
                                        .padding(1)
                                }
                            }
                            
                            Divider()
                                .padding()
                        }
//
//                        NavigationLink {
//                            NeighborhoodsListView(viewModel: viewModel.neighborhoodsViewModel)
//                        } label: {
//                            if viewModel.selectedNeighborhood != "" {
//                                Text(viewModel.selectedNeighborhood)
//                                    .foregroundStyle(.black)
//                                    .frame(maxWidth: .infinity, alignment: .leading)
//                                
//                            } else {
//                                HStack {
//                                    Text("Выбрать свой район")
//                                        .foregroundStyle(.black)
//                                        .frame(maxWidth: .infinity, alignment: .leading)
//                                    Image(systemName: "chevron.right")
//                                        .renderingMode(.template)
//                                        .foregroundStyle(.black)
//                                }
//                            }
//                        }
//                        .padding()
//                        .frame(maxHeight: 50)
//                        .background {
//                            RoundedRectangle(cornerRadius: 20)
//                                .stroke(LinearGradient(colors: [ .black, .black.opacity(0.3), .black], startPoint: .topLeading, endPoint: .bottomTrailing), lineWidth: 1)
//                                .padding(1)
//                        }

                    }
                    
                    
                    NavigationLink {
                        ScrollView {
                            Text(Constants.privacyPolicy)
                                .padding(.horizontal)
                        }
                    } label: {
                        HStack {
                            Text("Юридическая информация")
                                .foregroundStyle(.black)
                                .frame(maxWidth: .infinity, alignment: .leading)
                            Image(systemName: "chevron.right")
                                .renderingMode(.template)
                                .foregroundStyle(.black)
                        }
                        .padding()
                        .frame(maxHeight: 50)
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(LinearGradient(colors: [ .black, .black.opacity(0.3), .black], startPoint: .topLeading, endPoint: .bottomTrailing), lineWidth: 1)
                                .padding(1)
                        }
                    }
                    
                    NavigationLink {
                        BugReportView()
                    } label: {
                        HStack {
                            Text("Связаться с разработчиком")
                                .foregroundStyle(.black)
                                .frame(maxWidth: .infinity, alignment: .leading)
                            Image(systemName: "chevron.right")
                                .renderingMode(.template)
                                .foregroundStyle(.black)
                        }
                        .padding()
                        .frame(maxHeight: 50)
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(LinearGradient(colors: [ .black, .black.opacity(0.3), .black], startPoint: .topLeading, endPoint: .bottomTrailing), lineWidth: 1)
                                .padding(1)
                        }
                    }
                    
                    NavigationLink {
                        ScrollView {
                            Text(Constants.aboutApp)
                                .padding(.horizontal)
                        }
                    } label: {
                        HStack {
                            Text("О приложении")
                                .foregroundStyle(.black)
                                .frame(maxWidth: .infinity, alignment: .leading)
                            Image(systemName: "chevron.right")
                                .renderingMode(.template)
                                .foregroundStyle(.black)
                        }
                        .padding()
                        .frame(maxHeight: 50)
                        .background {
                            RoundedRectangle(cornerRadius: 20)
                                .stroke(LinearGradient(colors: [ .black, .black.opacity(0.3), .black], startPoint: .topLeading, endPoint: .bottomTrailing), lineWidth: 1)
                                .padding(1)
                        }
                    }
                }
            }
            .padding(.horizontal)
            .blur(radius: viewModel.isLoginViewVisible ? 3 : 0)

            loginView
        }
        .animation(.snappy, value: viewModel.isLogged)
        .animation(.snappy, value: viewModel.isLoginViewVisible)
        .navigationBar(with: viewModel.navigationBar)
    }
}

extension ProfileView {
//    var logButton: some View {
//        Button {
//            viewModel.loginAction()
//        } label: {
//            Text(viewModel.isLogged ? "Выйти" : "Войти")
//        }
//    }
    
    var loginView: some View {
        var offset: CGFloat = 0
        return LoginTypeSelectionView(viewModel: viewModel.loginTypeSelectionViewModel)
        .offset(y: viewModel.isLoginViewVisible ? 0 : 500)
        .frame(maxHeight: .infinity, alignment: .bottom)
        .gesture (
            DragGesture()
                .onChanged { gesture in
                    offset = gesture.translation.height
                }
                .onEnded{ _ in
                    if offset <= 500 {
                        viewModel.isLoginViewVisible = false
                    }
                }
        )
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(viewModel: .init(core: .emptyMock))
    }
}
