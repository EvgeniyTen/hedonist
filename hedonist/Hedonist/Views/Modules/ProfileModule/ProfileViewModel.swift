//
//  ProfileViewModel.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 25.07.2023.
//

import SwiftUI
import Combine

class ProfileViewModel: ObservableObject {
    private let core: Core
    @Published @MainActor var isLogged: Bool = true
    @Published var isLoginViewVisible: Bool = false
    @Published var userData: CustomerData = .notLoggedUser
    @Published var userNameValue: String = ""
    @Published var loginTypeSelectionViewModel: LoginTypeSelectionView.ViewModel
    @Published var neighborhoodsViewModel: NeighborhoodsListView.ViewModel
    @Published var navigationBar: NavigationBarView.ViewModel

    @Published var isNotificationsEnabled: Bool = false
    @Published var selectedNeighborhood: String = "" {
        willSet {
            neighborhoodsViewModel.selectedHeighbor = newValue
        }
    }
    @Published var neighborhoods: [String] = [] {
        willSet {
            self.neighborhoodsViewModel.neighborhoods = newValue
        }
    }
    private var filters: [String: Any] = [:]
    private var dispose = Set<AnyCancellable>()

    init(
        core: Core,
        loginTypeSelectionViewModel: LoginTypeSelectionView.ViewModel,
        neighborhoodsViewModel: NeighborhoodsListView.ViewModel,
        navigationBar: NavigationBarView.ViewModel
    ) {
        self.core = core
        self.loginTypeSelectionViewModel = loginTypeSelectionViewModel
        self.neighborhoodsViewModel = neighborhoodsViewModel
        self.navigationBar = navigationBar
        bind()

    }
    
    
    convenience init(core: Core) {
        self.init(core: core,
                  loginTypeSelectionViewModel: .init(core: core, successDismiss: {}, dismiss: {}),
                  neighborhoodsViewModel: .init(neighborhoods: [], onTap: { _ in }),
                  navigationBar: .init(title: .text("О приложении"),
                                       leftButtons: [NavigationBarView.ViewModel.BackButtonViewModel(action: {})])
        )
        self.loginTypeSelectionViewModel.successDismiss = {
            self.isLoginViewVisible = false
        }
        self.loginTypeSelectionViewModel.dismiss = {
            self.isLoginViewVisible = false
        }
        self.neighborhoodsViewModel.onTap = selectNeighborhood
    }
//    
//    func setRightNavigationBarButton() {
//        Task {
//            await MainActor.run {
//                self.navigationBar.rightButtons = [NavigationBarView.ViewModel.TextButtonViewModel(
//                    text: isLogged
//                    ? "Выйти"
//                    : "Войти",
//                    action: self.loginAction)]
//            }
//        }
//    }
    
    func bind() {
        notificationsCheckPermissions()
//        
//        core.userData
//            .receive(on: DispatchQueue.main)
//            .sink { [unowned self] data in
//                Task {
//                    await MainActor.run {
//                        userData = data
//                        isLogged = userData.id != "not_logged_user"
//                    }
//                }
//            }
//            .store(in: &dispose)
        
        
        core.restaurantProperties
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] properties in
                if let neighborhoodProperties = properties["neighborhood"] as? [String] {
                    neighborhoods.append(contentsOf: neighborhoodProperties)
                }
            }
            .store(in: &dispose)
        
        core.selectedFilters
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] properties in
                var localCuisinesContainer: [String] = []
                var localNeighborhoodValue: [String] = []
                var localDietaryContainer: [String] = []
                var localFeaturesContainer: [String] = []
                
                properties.forEach { cell in
                    
                    switch cell.type {
                    case .cuisines:
                        localCuisinesContainer.append(cell.title)
                        
                    case .neighborhood:
                        localNeighborhoodValue.append(cell.title)
                        
                    case .dietaryrestrictions:
                        localDietaryContainer.append(cell.title)
                        
                    case .features:
                        localFeaturesContainer.append(cell.title)
                        
                    }
                }
                
                filters["cuisines"] = localCuisinesContainer
                if localCuisinesContainer.isEmpty {
                    filters.removeValue(forKey: "cuisines")
                }
                filters["neighborhood"] = localNeighborhoodValue
                if let lastSelected = localNeighborhoodValue.last {
                    selectedNeighborhood = lastSelected
                }
                filters["dietaryrestrictions"] = localDietaryContainer
                if localDietaryContainer.isEmpty {
                    filters.removeValue(forKey: "dietaryrestrictions")
                }
                filters["features"] = localFeaturesContainer
                if localFeaturesContainer.isEmpty {
                    filters.removeValue(forKey: "features")
                }
            }
            .store(in: &dispose)

        core.restaurantProperties
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] properties in
                if let neighborhoodProperties = properties["neighborhood"] as? [String] {
                    neighborhoods.append(contentsOf: neighborhoodProperties)
                }
            }
            .store(in: &dispose)
        
        core.notificationStatus
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] state in
                isNotificationsEnabled = state
            }
            .store(in: &dispose)
        
    }
//
//    @MainActor
//    func loginAction() {
//        isLogged
//        ? killProfile()
//        : createProfile()
//    }
    
    // MARK: - Temporary solution
    func notificationsCheckPermissions() {
        core.action.send(CoreModelAction.Notifications.CheckPermissions())
    }
    
    func notificationsRequest() {
        core.action.send(CoreModelAction.Notifications.RequestPermissions())
    }
    
    @MainActor
    func selectNeighborhood(title: String) {
        selectedNeighborhood = title
        core.action.send(CoreModelAction.PersistantAction.UpdateValue(name: "neighborhood", filter: selectedNeighborhood))
        core.action.send(CoreModelAction.RestaurantManagerAction.GetNewItem.init(value: filters))
//        if isLogged {
//        } else {
//            isLoginViewVisible = true
//        }
    }
    
    func killProfile() {
        core.action.send(CoreModelAction.LoginAction.SignOut())
    }
    
    func createProfile() {
        isLoginViewVisible = true
    }
}
