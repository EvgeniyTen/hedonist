//
//  ContentView.swift
//  hedonist
//
//  Created by Evgeniy Timofeev on 16.04.2023.
//

import SwiftUI

struct ContentView: View {
    @AppStorage("wasOnboarded") var wasOnboarded: Bool = false
    var core: Core
    
    init(core: Core) {
        self.core = core
    }
    
    var body: some View {
        NavigationView {
            if wasOnboarded {
                MainView(viewModel: .init(core: core))
            } else {
                OnboardingView(viewModel: .init(core: core))
            }
        }.navigationViewStyle(.stack)
    }
    

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(core: .emptyMock)
    }
}
