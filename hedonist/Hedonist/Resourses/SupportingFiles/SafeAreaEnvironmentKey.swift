//
//  SafeAreaEnvironmentKey.swift
//  Hedonist
//
//  Created by Evgeniy Timofeev on 17.03.2024.
//

import SwiftUI

struct SafeAreaInsetsKey: EnvironmentKey {
    static var defaultValue: EdgeInsets {
        let keywindow = UIApplication
            .shared
            .connectedScenes
            .compactMap { $0 as? UIWindowScene }
            .flatMap(\.windows)
            .last(where: \.isKeyWindow)
        let safeAreaInsets = keywindow?.safeAreaInsets ?? .zero
        
        return safeAreaInsets.insets
    }
}

extension UIEdgeInsets {
    var insets: EdgeInsets {
        EdgeInsets(
            top: top,
            leading: left,
            bottom: bottom,
            trailing: right)
    }
}


extension EnvironmentValues {
    var safeAreaInsets: EdgeInsets {
        self[SafeAreaInsetsKey.self]
    }
}
